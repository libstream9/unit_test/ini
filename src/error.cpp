#include <stream9/ini/error.hpp>

#include <sstream>

#include <boost/test/unit_test.hpp>

namespace stream9::ini::testing {

BOOST_AUTO_TEST_SUITE(error_)

    BOOST_AUTO_TEST_CASE(stream_error_code_)
    {
        std::ostringstream oss;

        oss << error_code::invalid_char;

        BOOST_TEST(oss.str() == "invalid character");
    }

BOOST_AUTO_TEST_SUITE_END() // error_

} // namespace stream9::ini::testing
