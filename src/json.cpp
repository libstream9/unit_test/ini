#include "json.hpp"

#include <stream9/json.hpp>

namespace stream9::ini::testing {

static json::object
to_json(ini::section const& s)
{
    json::object obj;

    for (auto& [key, value]: s) {
        obj[key] = value;
    }

    return obj;
}

json::object
to_json(ini_file const& ini)
{
    json::object root;

    for (auto& section: ini) {
        auto obj = to_json(section);
        root[section.name()] = std::move(obj);
    }

    return root;
}

json::object
to_json(parse_error const& e)
{
    json::object obj;

    obj["code"] = to_string(e.code());
    obj["path"] = e.path().c_str();
    obj["line_no"] = e.line_no();
    obj["position"] = e.position();

    return obj;
}

} // namespace stream9::ini::testing
