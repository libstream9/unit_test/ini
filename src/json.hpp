#ifndef STREAM9_INI_TEST_JSON_HPP
#define STREAM9_INI_TEST_JSON_HPP

#include <string>

#include <stream9/ini/map/ini_file.hpp>

#include <stream9/json.hpp>

namespace stream9::ini::testing {

json::object to_json(ini_file const&);

json::object to_json(parse_error const&);

} // namespace stream9::ini::testing

#endif // STREAM9_INI_TEST_JSON_HPP
