#include <stream9/ini/editor.hpp>

#include <cstring>

#include <boost/test/unit_test.hpp>

namespace stream9::ini::editors::testing {

BOOST_AUTO_TEST_SUITE(editor_)
BOOST_AUTO_TEST_SUITE(section_)

    BOOST_AUTO_TEST_CASE(construct_from_parser_1_)
    {
        editor const e { "key = value" };
        auto const& s = e[""];

        BOOST_TEST(s.name() == "");
        BOOST_TEST(s.size() == 1);
        BOOST_TEST(s.top() == 0);
        BOOST_TEST(s.head() == 0);
        BOOST_TEST(s.bottom() == 12);

        BOOST_TEST(s.text() == "key = value\n");
    }

    BOOST_AUTO_TEST_CASE(construct_from_parser_2_)
    {
        editor const e { "[foo]\n"
                         "key = value" };
        auto const& s = e["foo"];

        BOOST_TEST(s.name() == "foo");
        BOOST_TEST(s.size() == 1);
        BOOST_TEST(s.top() == 0);
        BOOST_TEST(s.head() == 6);
        BOOST_TEST(s.bottom() == 18);

        BOOST_TEST(s.text() == "[foo]\n"
                               "key = value\n");
    }

    BOOST_AUTO_TEST_CASE(construct_from_editor_1_)
    {
        editor e;

        e[""]["key"] = "value";

        auto const& s = e[""];

        BOOST_TEST(s.name() == "");
        BOOST_TEST(s.size() == 1);
        BOOST_TEST(s.top() == 0);
        BOOST_TEST(s.head() == 0);
        BOOST_TEST(s.bottom() == 12);

        BOOST_TEST(s.text() == "key = value\n");
    }

    BOOST_AUTO_TEST_CASE(construct_from_editor_2_)
    {
        editor e;

        e["foo"]["key"] = "value";

        auto const& s = e["foo"];

        BOOST_TEST(s.name() == "foo");
        BOOST_TEST(s.size() == 1);
        BOOST_TEST(s.top() == 0);
        BOOST_TEST(s.head() == 6);
        BOOST_TEST(s.bottom() == 18);

        BOOST_TEST(s.text() == "[foo]\n"
                               "key = value\n");
    }

    BOOST_AUTO_TEST_CASE(move_)
    {
        editor e { "key = value" };

        auto& s1 = e[""];

        auto s2 = std::move(s1);

        BOOST_TEST(s2.size() == 1);
        //TODO what about move from object? delete from editor also?
    }

    BOOST_AUTO_TEST_CASE(find_)
    {
        editor e { "key = value" };
        auto& s = e[""];

        auto* const v = s.find("key");
        BOOST_TEST(v);
        BOOST_TEST(*v == "value");
    }

    BOOST_AUTO_TEST_CASE(find_const_)
    {
        editor const e { "key = value" };
        auto& s = e[""];

        auto* const v = s.find("key");
        BOOST_TEST(v);
        BOOST_TEST(*v == "value");
    }

    BOOST_AUTO_TEST_CASE(find_or_fallback_1_)
    {
        editor const e { "key = value" };
        auto& s = e[""];

        auto const v = s.find("key", "fallback");
        BOOST_TEST(v == "value");
    }

    BOOST_AUTO_TEST_CASE(find_or_fallback_2_)
    {
        editor const e { "key = value" };
        auto& s = e[""];

        auto const v = s.find("kez", "fallback");
        BOOST_TEST(v == "fallback");
    }

    BOOST_AUTO_TEST_CASE(find_or_throw_1_)
    {
        editor e { "key = value" };
        auto& s = e[""];

        auto const& v = s.find_or_throw("key");
        BOOST_TEST(v == "value");
    }

    BOOST_AUTO_TEST_CASE(find_or_throw_2_)
    {
        editor e { "key = value" };
        auto& s = e[""];

        BOOST_CHECK_EXCEPTION(
            s.find_or_throw("kez"),
            error,
            [&](auto& e) {
                BOOST_TEST(e.code() == error_code::no_such_key);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(find_or_throw_const_1_)
    {
        editor const e { "key = value" };
        auto& s = e[""];

        auto const& v = s.find_or_throw("key");
        BOOST_TEST(v == "value");
    }

    BOOST_AUTO_TEST_CASE(find_or_throw_const_2_)
    {
        editor const e { "key = value" };
        auto& s = e[""];

        BOOST_CHECK_EXCEPTION(
            s.find_or_throw("kez"),
            error,
            [&](auto& e) {
                BOOST_TEST(e.code() == error_code::no_such_key);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(contains_1_)
    {
        editor const e { "key = value" };
        auto& s = e[""];

        BOOST_TEST(s.contains("key"));
    }

    BOOST_AUTO_TEST_CASE(contains_2_)
    {
        editor const e { "key = value" };
        auto& s = e[""];

        BOOST_TEST(!s.contains("kez"));
    }

    BOOST_AUTO_TEST_CASE(append_1_)
    {
        editor e;
        auto& s = e[""];

        s.append("key", "value");

        BOOST_TEST(s.text() == "key = value\n");
    }

    BOOST_AUTO_TEST_CASE(append_2_)
    {
        editor e;
        auto& s = e["foo"];

        s.append("key1", "value1");
        s.append("key2", "value2");

        auto const expected = "[foo]\n"
                              "key1 = value1\n"
                              "key2 = value2\n";
        BOOST_TEST(s.text() == expected);
    }

    BOOST_AUTO_TEST_CASE(find_or_append_1_)
    {
        editor e { "key = value" };
        auto& s = e[""];

        auto& v = s.find_or_append("key");

        BOOST_TEST(v == "value");
        BOOST_TEST(s.text() == "key = value\n");
    }

    BOOST_AUTO_TEST_CASE(find_or_append_2_)
    {
        editor e { "key = value" };
        auto& s = e[""];

        auto& v = s.find_or_append("foo");

        BOOST_TEST(v == "");
        BOOST_TEST(s.text() == "key = value\n"
                               "foo = \n");
    }

    BOOST_AUTO_TEST_CASE(erase_1_)
    {
        editor e { "key1 = value1\n"
                   "key2 = value2" };
        auto& s = e[""];

        BOOST_TEST(s.erase("key1"));
        BOOST_TEST(s.size() == 1);
        BOOST_TEST(s.text() == "key2 = value2\n");
    }

    BOOST_AUTO_TEST_CASE(erase_2_)
    {
        editor e { "key = value" };
        auto& s = e[""];

        BOOST_TEST(s.erase("key"));
        BOOST_TEST(s.empty());
        BOOST_TEST(s.text() == "");
    }

    BOOST_AUTO_TEST_CASE(erase_3_)
    {
        editor e { "key = value" };
        auto& s = e[""];

        BOOST_TEST(!s.erase("foo"));
        BOOST_TEST(s.size() == 1);
        BOOST_TEST(s.text() == "key = value\n");
    }

    BOOST_AUTO_TEST_CASE(rename_from_anonymous_)
    {
        editor e { "key1 = value1\n"
                   "[bar]\n"
                   "key2 = value2" };
        auto& s = e[0];

        s.rename("foo");

        BOOST_TEST(s.name() == "foo");
        BOOST_TEST(s.text() == "[foo]\n"
                               "key1 = value1\n");

        BOOST_TEST(e.text() == "[foo]\n"
                               "key1 = value1\n"
                               "[bar]\n"
                               "key2 = value2\n");
    }

    BOOST_AUTO_TEST_CASE(rename_to_anonymous_1_)
    {
        editor e { "[foo]\n"
                   "key1 = value1\n"
                   "[bar]\n"
                   "key2 = value2" };
        auto& s = e[0];

        s.rename("");

        BOOST_TEST(s.name() == "");
        BOOST_TEST(s.text() == "key1 = value1\n");

        BOOST_TEST(e.text() == "key1 = value1\n"
                               "[bar]\n"
                               "key2 = value2\n");
    }

    BOOST_AUTO_TEST_CASE(rename_to_anonymous_2_)
    {
        editor e { "[foo]\n"
                   "key1 = value1\n"
                   "[bar]\n"
                   "key2 = value2" };
        auto& s = e[1];

        s.rename("");

        BOOST_TEST(s.name() == "");
        BOOST_TEST(s.text() == "key2 = value2\n");

        BOOST_TEST(e.text() == "key2 = value2\n"
                               "[foo]\n"
                               "key1 = value1\n");
    }

    BOOST_AUTO_TEST_CASE(rename_to_longer_name_)
    {
        editor e { "[foo]\n"
                   "key = value" };
        auto& s = e[0];

        s.rename("xyzzy");

        BOOST_TEST(s.name() == "xyzzy");

        auto const expected = "[xyzzy]\n"
                              "key = value\n";
        BOOST_TEST(s.text() == expected);
        BOOST_TEST(e.text() == expected);
    }

    BOOST_AUTO_TEST_CASE(rename_to_shorter_name_)
    {
        editor e { "[foo]\n"
                   "key = value" };
        auto& s = e[0];

        s.rename("X");

        BOOST_TEST(s.name() == "X");

        auto const expected = "[X]\n"
                              "key = value\n";
        BOOST_TEST(s.text() == expected);
        BOOST_TEST(e.text() == expected);
    }

    BOOST_AUTO_TEST_CASE(clear_)
    {
        editor e { "key = value\n"
                   "foo = 1\n"
                   "bar = 2\n"
                   "[foo]\n"
                   "x = 6" };
        auto& s = e[""];

        s.clear();

        BOOST_TEST(s.empty());
        BOOST_TEST(s.text() == "");

        BOOST_TEST(e.text() == "[foo]\n"
                               "x = 6\n");
    }

    BOOST_AUTO_TEST_CASE(stream_out_)
    {
        editor const e { "[foo]\n"
                         "key1 = value1" };
        auto& s = e["foo"];

        std::ostringstream oss;
        oss << s;

        BOOST_TEST(oss.str() == "[foo]\n"
                                "key1 = value1\n");
    }

BOOST_AUTO_TEST_SUITE_END() // section_
BOOST_AUTO_TEST_SUITE_END() // editor_

} // namespace stream9::ini::editors::testing
