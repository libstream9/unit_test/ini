#include <stream9/ini/editor.hpp>

#include <boost/test/unit_test.hpp>

namespace stream9::ini::testing::editors {

using namespace stream9::ini::editors;

BOOST_AUTO_TEST_SUITE(buffer_)

BOOST_AUTO_TEST_SUITE(insert_text_)

    BOOST_AUTO_TEST_CASE(empty_)
    {
        buffer buf;
        auto m1 = buf.add_mark(0);

        buf.insert_text(0, "");

        BOOST_TEST(buf.text() == "");
        BOOST_TEST(buf.line_count() == 0);
        BOOST_TEST(buf.beginning_of_line(0) == 0);
        BOOST_TEST(*m1 == 0);
    }

    BOOST_AUTO_TEST_CASE(insert_one_line_to_empty_)
    {
        buffer buf;

        buf.insert_text(0, "foo");

        BOOST_TEST(buf.text() == "foo\n");
        BOOST_TEST(buf.line_count() == 1);
        BOOST_TEST(buf.beginning_of_line(0) == 0);
        BOOST_TEST(buf.beginning_of_line(1) == 4);
    }

    BOOST_AUTO_TEST_CASE(insert_two_lines_to_empty_)
    {
        buffer buf;

        buf.insert_text(0, "line1\nline2\n");

        BOOST_TEST(buf.text() == "line1\nline2\n");
        BOOST_TEST(buf.line_count() == 2);
        BOOST_TEST(buf.beginning_of_line(0) == 0);
        BOOST_TEST(buf.beginning_of_line(1) == 6);
        BOOST_TEST(buf.beginning_of_line(2) == 12);
    }

    BOOST_AUTO_TEST_CASE(insert_one_line_to_back_)
    {
        buffer buf { "line1" };
        auto m1 = buf.add_mark(0);
        auto m2 = buf.add_mark(6);

        buf.insert_text(6, "line2");

        BOOST_TEST(buf.text() == "line1\nline2\n");

        BOOST_TEST(buf.line_count() == 2);
        BOOST_TEST(buf.beginning_of_line(0) == 0);
        BOOST_TEST(buf.beginning_of_line(1) == 6);
        BOOST_TEST(buf.beginning_of_line(2) == 12);

        BOOST_TEST(*m1 == 0);
        BOOST_TEST(*m2 == 12);
    }

    BOOST_AUTO_TEST_CASE(insert_one_line_to_front_)
    {
        buffer buf { "line2" };
        auto m1 = buf.add_mark(0);
        auto m2 = buf.add_mark(6);

        buf.insert_text(0, "line1\n");

        BOOST_TEST(buf.text() == "line1\nline2\n");

        BOOST_TEST(buf.line_count() == 2);
        BOOST_TEST(buf.beginning_of_line(0) == 0);
        BOOST_TEST(buf.beginning_of_line(1) == 6);
        BOOST_TEST(buf.beginning_of_line(2) == 12);

        BOOST_TEST(*m1 == 6);
        BOOST_TEST(*m2 == 12);
    }

    BOOST_AUTO_TEST_CASE(insert_one_line_to_middle_)
    {
        buffer buf { "line1\nline3" };
        auto m1 = buf.add_mark(0);
        auto m2 = buf.add_mark(6);
        auto m3 = buf.add_mark(12);

        buf.insert_text(6, "line2\n");

        BOOST_TEST(buf.text() == "line1\nline2\nline3\n");

        BOOST_TEST(buf.line_count() == 3);
        BOOST_TEST(buf.beginning_of_line(0) == 0);
        BOOST_TEST(buf.beginning_of_line(1) == 6);
        BOOST_TEST(buf.beginning_of_line(2) == 12);
        BOOST_TEST(buf.beginning_of_line(3) == 18);

        BOOST_TEST(*m1 == 0);
        BOOST_TEST(*m2 == 12);
        BOOST_TEST(*m3 == 18);
    }

    BOOST_AUTO_TEST_CASE(insert_two_lines_to_back_)
    {
        buffer buf { "line1" };
        auto m1 = buf.add_mark(0);
        auto m2 = buf.add_mark(6);

        buf.insert_text(6, "line2\nline3");

        BOOST_TEST(buf.text() == "line1\nline2\nline3\n");

        BOOST_TEST(buf.line_count() == 3);
        BOOST_TEST(buf.beginning_of_line(0) == 0);
        BOOST_TEST(buf.beginning_of_line(1) == 6);
        BOOST_TEST(buf.beginning_of_line(2) == 12);
        BOOST_TEST(buf.beginning_of_line(3) == 18);

        BOOST_TEST(*m1 == 0);
        BOOST_TEST(*m2 == 18);
    }

    BOOST_AUTO_TEST_CASE(insert_two_lines_to_front_)
    {
        buffer buf { "line3" };
        auto m1 = buf.add_mark(0);
        auto m2 = buf.add_mark(6);

        buf.insert_text(0, "line1\nline2\n");

        BOOST_TEST(buf.text() == "line1\nline2\nline3\n");

        BOOST_TEST(buf.line_count() == 3);
        BOOST_TEST(buf.beginning_of_line(0) == 0);
        BOOST_TEST(buf.beginning_of_line(1) == 6);
        BOOST_TEST(buf.beginning_of_line(2) == 12);
        BOOST_TEST(buf.beginning_of_line(3) == 18);

        BOOST_TEST(*m1 == 12);
        BOOST_TEST(*m2 == 18);
    }

    BOOST_AUTO_TEST_CASE(insert_two_lines_to_middle_)
    {
        buffer buf { "line1\nline4" };
        auto m1 = buf.add_mark(0);
        auto m2 = buf.add_mark(6);
        auto m3 = buf.add_mark(12);

        buf.insert_text(6, "line2\nline3\n");

        BOOST_TEST(buf.text() == "line1\nline2\nline3\nline4\n");

        BOOST_TEST(buf.line_count() == 4);
        BOOST_TEST(buf.beginning_of_line(0) == 0);
        BOOST_TEST(buf.beginning_of_line(1) == 6);
        BOOST_TEST(buf.beginning_of_line(2) == 12);
        BOOST_TEST(buf.beginning_of_line(3) == 18);
        BOOST_TEST(buf.beginning_of_line(4) == 24);

        BOOST_TEST(*m1 == 0);
        BOOST_TEST(*m2 == 18);
        BOOST_TEST(*m3 == 24);
    }

    BOOST_AUTO_TEST_CASE(insert_chars_into_middle_of_line_)
    {
        buffer buf { "abcde" };
        auto m1 = buf.add_mark(0);
        auto m2 = buf.add_mark(6);

        buf.insert_text(2, "ABCDE");

        BOOST_TEST(buf.line_count() == 1);
        BOOST_TEST(buf.beginning_of_line(0) == 0);
        BOOST_TEST(buf.beginning_of_line(1) == 11);

        BOOST_TEST(buf.text() == "abABCDEcde\n");
        BOOST_TEST(*m1 == 0);
        BOOST_TEST(*m2 == 11);
    }

    BOOST_AUTO_TEST_CASE(insert_lines_into_middle_of_line_)
    {
        buffer buf { "xxyy" };
        auto m1 = buf.add_mark(0);
        auto m2 = buf.add_mark(5);

        buf.insert_text(2, "line1\nline2\n");

        BOOST_TEST(buf.text() == "xxline1\nline2\nyy\n");

        BOOST_TEST(buf.line_count() == 3);
        BOOST_TEST(buf.beginning_of_line(0) == 0);
        BOOST_TEST(buf.beginning_of_line(1) == 8);
        BOOST_TEST(buf.beginning_of_line(2) == 14);

        BOOST_TEST(*m1 == 0);
        BOOST_TEST(*m2 == 17);
    }

BOOST_AUTO_TEST_SUITE_END() // insert_text_

BOOST_AUTO_TEST_SUITE(remove_text_)

    BOOST_AUTO_TEST_CASE(empty_)
    {
        buffer buf;
        auto m1 = buf.add_mark(0);

        buf.remove_text(0, 0);

        BOOST_TEST(*m1 == 0);
    }

    BOOST_AUTO_TEST_CASE(one_line_to_empty_)
    {
        buffer buf { "line1" };
        auto m1 = buf.add_mark(0);
        auto m2 = buf.add_mark(6);

        buf.remove_text(0, 6);

        BOOST_TEST(buf.text() == "");
        BOOST_TEST(*m1 == 0);
        BOOST_TEST(*m2 == 0);
    }

    BOOST_AUTO_TEST_CASE(one_line_from_top_)
    {
        buffer buf { "line1\nline2" };
        auto m1 = buf.add_mark(0);
        auto m2 = buf.add_mark(6);
        auto m3 = buf.add_mark(12);

        buf.remove_text(0, 6);

        BOOST_TEST(buf.text() == "line2\n");
        BOOST_TEST(*m1 == 0);
        BOOST_TEST(*m2 == 0);
        BOOST_TEST(*m3 == 6);
    }

    BOOST_AUTO_TEST_CASE(one_line_frobottom_)
    {
        buffer buf { "line1\nline2" };
        auto m1 = buf.add_mark(0);
        auto m2 = buf.add_mark(6);
        auto m3 = buf.add_mark(12);

        buf.remove_text(6, 6);

        BOOST_TEST(buf.text() == "line1\n");
        BOOST_TEST(*m1 == 0);
        BOOST_TEST(*m2 == 6);
        BOOST_TEST(*m3 == 6);
    }

    BOOST_AUTO_TEST_CASE(one_line_from_middle_)
    {
        buffer buf { "line1\nline2\nline3" };
        auto m1 = buf.add_mark(0);
        auto m2 = buf.add_mark(6);
        auto m3 = buf.add_mark(12);
        auto m4 = buf.add_mark(18);

        buf.remove_text(6, 6);

        BOOST_TEST(buf.text() == "line1\nline3\n");
        BOOST_TEST(*m1 == 0);
        BOOST_TEST(*m2 == 6);
        BOOST_TEST(*m3 == 6);
        BOOST_TEST(*m4 == 12);
    }

BOOST_AUTO_TEST_SUITE_END() // remove_text_

BOOST_AUTO_TEST_SUITE_END() // buffer_

} // namespace stream9::ini::testing::editors
