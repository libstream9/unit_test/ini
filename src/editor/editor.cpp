#include <stream9/ini/editor.hpp>

#include "../file.hpp"

#include <fstream>
#include <sstream>

#include <boost/test/unit_test.hpp>

//TODO error

using namespace stream9::ini;
using namespace stream9::ini::editors;

BOOST_AUTO_TEST_SUITE(editor_)

    BOOST_AUTO_TEST_CASE(default_construction_)
    {
        editor const e;

        BOOST_TEST(e.empty());
    }

    BOOST_AUTO_TEST_CASE(construction_from_text_)
    {
        auto const text = "key1 = value1\n";

        editor e { text };

        BOOST_TEST(e.section_count() == 1);
        BOOST_TEST(e.contains_section(""));

        auto const& s = e[""];
        BOOST_TEST(s.size() == 1);
        BOOST_TEST(s["key1"] == "value1");
    }

    BOOST_AUTO_TEST_CASE(find_section_)
    {
        auto const text = "[section1]\n"
                          "key = value2";

        editor e { text };

        auto* const s = e.find_section("section1");
        BOOST_TEST(s->name() == "section1");
    }

    BOOST_AUTO_TEST_CASE(find_section_const_)
    {
        auto const text = "[section1]\n"
                          "key = value2";

        editor const e { text };

        auto* const s = e.find_section("section1");
        BOOST_TEST(s->name() == "section1");
    }

    BOOST_AUTO_TEST_CASE(find_section_or_throw_)
    {
        auto const text = "[section1]\n"
                          "key = value2";

        editor e { text };

        auto& s = e.find_section_or_throw("section1");
        BOOST_TEST(s.name() == "section1");

        BOOST_CHECK_EXCEPTION(
            e.find_section_or_throw(""),
            error,
            [&](auto& e) {
                BOOST_TEST(e.code() == error_code::no_such_section);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(find_section_or_throw_const_)
    {
        auto const text = "[section1]\n"
                          "key = value2";

        editor const e { text };

        auto const& s = e.find_section_or_throw("section1");
        BOOST_TEST(s.name() == "section1");

        BOOST_CHECK_EXCEPTION(
            e.find_section_or_throw(""),
            error,
            [&](auto& e) {
                BOOST_TEST(e.code() == error_code::no_such_section);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(find_value_1_)
    {
        editor e { "[foo]\n"
                   "key1 = value1" };

        auto* const v = e.find_value("foo", "key1");

        BOOST_REQUIRE(v);
        BOOST_TEST(*v == "value1");
    }

    BOOST_AUTO_TEST_CASE(find_value_2_)
    {
        editor e { "[foo]\n"
                   "key1 = value1" };

        auto* const v = e.find_value("bar", "key1");

        BOOST_TEST(!v);
    }

    BOOST_AUTO_TEST_CASE(find_value_3_)
    {
        editor e { "[foo]\n"
                   "key1 = value1" };

        auto* const v = e.find_value("foo", "key2");

        BOOST_TEST(!v);
    }

    BOOST_AUTO_TEST_CASE(find_value_const_1_)
    {
        editor const e { "[foo]\n"
                   "key1 = value1" };

        auto* const v = e.find_value("foo", "key1");

        BOOST_REQUIRE(v);
        BOOST_TEST(*v == "value1");
    }

    BOOST_AUTO_TEST_CASE(find_value_const_2_)
    {
        editor const e { "[foo]\n"
                   "key1 = value1" };

        auto* const v = e.find_value("bar", "key1");

        BOOST_TEST(!v);
    }

    BOOST_AUTO_TEST_CASE(find_value_const_3_)
    {
        editor const e { "[foo]\n"
                   "key1 = value1" };

        auto* const v = e.find_value("foo", "key2");

        BOOST_TEST(!v);
    }

    BOOST_AUTO_TEST_CASE(find_value_with_fallback_1_)
    {
        editor const e { "[foo]\n"
                   "key1 = value1" };

        auto const v = e.find_value("foo", "key1", "fallback");

        BOOST_TEST(v == "value1");
    }

    BOOST_AUTO_TEST_CASE(find_value_with_fallback_2_)
    {
        editor const e { "[foo]\n"
                   "key1 = value1" };

        auto const v = e.find_value("bar", "key1", "fallback");

        BOOST_TEST(v == "fallback");
    }

    BOOST_AUTO_TEST_CASE(find_value_with_fallback_3_)
    {
        editor const e { "[foo]\n"
                   "key1 = value1" };

        auto const v = e.find_value("foo", "key2", "fallback");

        BOOST_TEST(v == "fallback");
    }

    BOOST_AUTO_TEST_CASE(find_value_or_throw_1_)
    {
        editor e { "[foo]\n"
                   "key1 = value1" };

        auto& v = e.find_value_or_throw("foo", "key1");

        BOOST_TEST(v == "value1");
    }

    BOOST_AUTO_TEST_CASE(find_value_or_throw_2_)
    {
        editor e { "[foo]\n"
                   "key1 = value1" };

        BOOST_CHECK_EXCEPTION(
            e.find_value_or_throw("bar", "key1"),
            error,
            [&](auto& e) {
                BOOST_TEST(e.code() == error_code::no_such_section);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(find_value_or_throw_3_)
    {
        editor e { "[foo]\n"
                   "key1 = value1" };

        BOOST_CHECK_EXCEPTION(
            e.find_value_or_throw("foo", "key2"),
            error,
            [&](auto& e) {
                BOOST_TEST(e.code() == error_code::no_such_key);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(find_value_or_throw_const_1_)
    {
        editor const e { "[foo]\n"
                         "key1 = value1" };

        auto const& v = e.find_value_or_throw("foo", "key1");

        BOOST_TEST(v == "value1");
    }

    BOOST_AUTO_TEST_CASE(find_value_or_throw_const_2_)
    {
        editor const e { "[foo]\n"
                         "key1 = value1" };

        BOOST_CHECK_EXCEPTION(
            e.find_value_or_throw("bar", "key1"),
            error,
            [&](auto& e) {
                BOOST_TEST(e.code() == error_code::no_such_section);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(find_value_or_throw_const_3_)
    {
        editor const e { "[foo]\n"
                         "key1 = value1" };

        BOOST_CHECK_EXCEPTION(
            e.find_value_or_throw("foo", "key2"),
            error,
            [&](auto& e) {
                BOOST_TEST(e.code() == error_code::no_such_key);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(contain_section_)
    {
        auto const text = "[section1]\n"
                          "key = value2";

        editor const e { text };

        BOOST_TEST(e.contains_section("section1"));
        BOOST_TEST(!e.contains_section("section2"));
    }

    BOOST_AUTO_TEST_CASE(section_count_1_)
    {
        editor const e;

        BOOST_TEST(e.section_count() == 0);
    }

    BOOST_AUTO_TEST_CASE(section_count_2_)
    {
        auto const text = "key = value1\n";

        editor const e { text };

        BOOST_TEST(e.section_count() == 1);
    }

    BOOST_AUTO_TEST_CASE(section_count_3_)
    {
        auto const text = "[section1]\n"
                          "key = value2";

        editor const e { text };

        BOOST_TEST(e.section_count() == 1);
    }

    BOOST_AUTO_TEST_CASE(section_count_4_)
    {
        auto const text = "key = value1\n"
                          "[section1]\n"
                          "key = value2";

        editor const e { text };

        BOOST_TEST(e.section_count() == 2);
    }

    BOOST_AUTO_TEST_CASE(empty_1_)
    {
        editor const e;

        BOOST_TEST(e.empty());
    }

    BOOST_AUTO_TEST_CASE(empty_2_)
    {
        auto const text = "key = value1\n";

        editor const e { text };

        BOOST_TEST(!e.empty());
    }

    BOOST_AUTO_TEST_CASE(assign_text_)
    {
        auto const text = "key = value1\n";

        editor e { text };

        BOOST_TEST(e[""]["key"] == "value1");

        e.assign_text("key = value2");

        BOOST_TEST(e[""]["key"] == "value2");
    }

    BOOST_AUTO_TEST_CASE(create_section_1_)
    {
        editor e;

        auto& s = e.create_section("");
        BOOST_TEST(s.name() == "");

        BOOST_TEST(e.section_count() == 1);

        BOOST_TEST(e.text() == "");
    }

    BOOST_AUTO_TEST_CASE(create_section_2_)
    {
        editor e;

        auto& s = e.create_section("foo");
        BOOST_TEST(s.name() == "foo");

        BOOST_TEST(e.section_count() == 1);

        BOOST_TEST(e.text() == "[foo]\n");
    }

    BOOST_AUTO_TEST_CASE(create_section_3_)
    {
        editor e { "key = value" };

        auto& s = e.create_section("foo");
        BOOST_TEST(s.name() == "foo");

        BOOST_TEST(e.section_count() == 2);

        auto const expected = "key = value\n"
                              "\n"
                              "[foo]\n";
        BOOST_TEST(e.text() == expected);
    }

    BOOST_AUTO_TEST_CASE(create_section_4_)
    {
        editor e { "[foo]\n"
                   "key = value" };

        auto& s = e.create_section("");
        BOOST_TEST(s.name() == "");

        BOOST_TEST(e.section_count() == 2);

        s["x"] = "y";

        auto const expected = "x = y\n"
                              "\n"
                              "[foo]\n"
                              "key = value\n";
        BOOST_TEST(e.text() == expected);
    }

    BOOST_AUTO_TEST_CASE(create_section_5_)
    {
        editor e { "[foo]\n"
                   "key = value" };

        auto& s = e.create_section("bar");
        BOOST_TEST(s.name() == "bar");

        BOOST_TEST(e.section_count() == 2);

        s["x"] = "y";

        auto const expected = "[foo]\n"
                              "key = value\n"
                              "\n"
                              "[bar]\n"
                              "x = y\n";
        BOOST_TEST(e.text() == expected);
    }

    BOOST_AUTO_TEST_CASE(find_or_create_section_1_)
    {
        editor e { "[foo]\n"
                   "key = value" };

        auto& s = e.find_or_create_section("foo");

        BOOST_TEST(e.section_count() == 1);
        BOOST_TEST(s.name() == "foo");
    }

    BOOST_AUTO_TEST_CASE(find_or_create_section_2_)
    {
        editor e;

        auto& s = e.find_or_create_section("foo");

        BOOST_TEST(e.section_count() == 1);
        BOOST_TEST(s.name() == "foo");

        auto const expected = "[foo]\n";
        BOOST_TEST(e.text() == expected);
    }

    BOOST_AUTO_TEST_CASE(move_section_top_to_bottom_)
    {
        editor e { "[foo]\n"  // 0 - 5
                   "x = 1\n"  // 6 - 11
                   "\n"       // 12
                   "[bar]\n"  // 13 - 18
                   "y = 2" }; // 19 - 23
        auto& s = e["foo"];

        e.move_section(s, 1);

        BOOST_TEST(e.section_count() == 2);
        BOOST_TEST(e[0].name() == "bar");
        BOOST_TEST(e[0].top() == 0);
        BOOST_TEST(e[0].head() == 6);
        BOOST_TEST(e[0].bottom() == 12);
        BOOST_TEST(e[1].name() == "foo");
        BOOST_TEST(e[1].top() == 12);
        BOOST_TEST(e[1].head() == 18);
        BOOST_TEST(e[1].bottom() == 25);

        BOOST_TEST(e.text() == "[bar]\n"  // 0 - 5
                               "y = 2\n"  // 6 - 11
                               "[foo]\n"  // 12 - 17
                               "x = 1\n"  // 18 - 23
                               "\n");     // 24 - 24
    }

    BOOST_AUTO_TEST_CASE(move_section_bottom_to_top_)
    {
        editor e { "[foo]\n"
                   "x = 1\n"
                   "\n"
                   "[bar]\n"
                   "y = 2" };
        auto& s = e["bar"];

        e.move_section(s, 0);

        BOOST_TEST(e.section_count() == 2);
        BOOST_TEST(e[0].name() == "bar");
        BOOST_TEST(e[0].top() == 0);
        BOOST_TEST(e[0].head() == 6);
        BOOST_TEST(e[0].bottom() == 12);
        BOOST_TEST(e[1].name() == "foo");
        BOOST_TEST(e[1].top() == 12);
        BOOST_TEST(e[1].head() == 18);
        BOOST_TEST(e[1].bottom() == 25);

        BOOST_TEST(e.text() == "[bar]\n"
                               "y = 2\n"
                               "[foo]\n"
                               "x = 1\n"
                               "\n");
    }

    BOOST_AUTO_TEST_CASE(move_section_top_to_middle_)
    {
        editor e { "[foo]\n"
                   "x = 1\n"
                   "\n"
                   "[bar]\n"
                   "y = 2\n"
                   "[xyzzy]\n"
                   "z = 5" };
        auto& s = e["foo"];

        e.move_section(s, 1);

        BOOST_TEST(e.section_count() == 3);
        BOOST_TEST(e[0].name() == "bar");
        BOOST_TEST(e[0].top() == 0);
        BOOST_TEST(e[0].head() == 6);
        BOOST_TEST(e[0].bottom() == 12);
        BOOST_TEST(e[1].name() == "foo");
        BOOST_TEST(e[1].top() == 12);
        BOOST_TEST(e[1].head() == 18);
        BOOST_TEST(e[1].bottom() == 25);
        BOOST_TEST(e[2].name() == "xyzzy");
        BOOST_TEST(e[2].top() == 25);
        BOOST_TEST(e[2].head() == 33);
        BOOST_TEST(e[2].bottom() == 39);

        BOOST_TEST(e.text() == "[bar]\n"
                               "y = 2\n"
                               "[foo]\n"
                               "x = 1\n"
                               "\n"
                               "[xyzzy]\n"
                               "z = 5\n");
    }

    BOOST_AUTO_TEST_CASE(move_section_bottom_to_middle_)
    {
        editor e { "[foo]\n"
                   "x = 1\n"
                   "\n"
                   "[bar]\n"
                   "y = 2\n"
                   "[xyzzy]\n"
                   "z = 5" };
        auto& s = e["xyzzy"];

        e.move_section(s, 1);

        BOOST_TEST(e.section_count() == 3);
        BOOST_TEST(e[0].name() == "foo");
        BOOST_TEST(e[0].top() == 0);
        BOOST_TEST(e[0].head() == 6);
        BOOST_TEST(e[0].bottom() == 13);
        BOOST_TEST(e[1].name() == "xyzzy");
        BOOST_TEST(e[1].top() == 13);
        BOOST_TEST(e[1].head() == 21);
        BOOST_TEST(e[1].bottom() == 27);
        BOOST_TEST(e[2].name() == "bar");
        BOOST_TEST(e[2].top() == 27);
        BOOST_TEST(e[2].head() == 33);
        BOOST_TEST(e[2].bottom() == 39);

        BOOST_TEST(e.text() == "[foo]\n"
                               "x = 1\n"
                               "\n"
                               "[xyzzy]\n"
                               "z = 5\n"
                               "[bar]\n"
                               "y = 2\n"
                               );
    }

    BOOST_AUTO_TEST_CASE(move_section_middle_to_top_)
    {
        editor e { "[foo]\n"
                   "x = 1\n"
                   "\n"
                   "[bar]\n"
                   "y = 2\n"
                   "[xyzzy]\n"
                   "z = 5" };
        auto& s = e["bar"];

        e.move_section(s, 0);

        BOOST_TEST(e.section_count() == 3);
        BOOST_TEST(e[0].name() == "bar");
        BOOST_TEST(e[0].top() == 0);
        BOOST_TEST(e[0].head() == 6);
        BOOST_TEST(e[0].bottom() == 12);
        BOOST_TEST(e[1].name() == "foo");
        BOOST_TEST(e[1].top() == 12);
        BOOST_TEST(e[1].head() == 18);
        BOOST_TEST(e[1].bottom() == 25);
        BOOST_TEST(e[2].name() == "xyzzy");
        BOOST_TEST(e[2].top() == 25);
        BOOST_TEST(e[2].head() == 33);
        BOOST_TEST(e[2].bottom() == 39);

        BOOST_TEST(e.text() ==
            "[bar]\n"
            "y = 2\n"
            "[foo]\n"
            "x = 1\n"
            "\n"
            "[xyzzy]\n"
            "z = 5\n"
            );
    }

    BOOST_AUTO_TEST_CASE(move_section_middle_to_bottom_)
    {
        editor e { "[foo]\n"
                   "x = 1\n"
                   "\n"
                   "[bar]\n"
                   "y = 2\n"
                   "[xyzzy]\n"
                   "z = 5" };
        auto& s = e["bar"];

        e.move_section(s, 2);

        BOOST_TEST(e.section_count() == 3);
        BOOST_TEST(e[0].name() == "foo");
        BOOST_TEST(e[0].top() == 0);
        BOOST_TEST(e[0].head() == 6);
        BOOST_TEST(e[0].bottom() == 13);
        BOOST_TEST(e[1].name() == "xyzzy");
        BOOST_TEST(e[1].top() == 13);
        BOOST_TEST(e[1].head() == 21);
        BOOST_TEST(e[1].bottom() == 27);
        BOOST_TEST(e[2].name() == "bar");
        BOOST_TEST(e[2].top() == 27);
        BOOST_TEST(e[2].head() == 33);
        BOOST_TEST(e[2].bottom() == 39);

        BOOST_TEST(e.text() ==
            "[foo]\n"
            "x = 1\n"
            "\n"
            "[xyzzy]\n"
            "z = 5\n"
            "[bar]\n"
            "y = 2\n"
            );
    }

    BOOST_AUTO_TEST_CASE(erase_section_1_)
    {
        editor e { "key = value" };

        auto const ok = e.erase_section("");

        BOOST_REQUIRE(ok);
        BOOST_TEST(e.section_count() == 0);
        BOOST_TEST(e.text() == "");
    }

    BOOST_AUTO_TEST_CASE(erase_section_2_)
    {
        editor e { "[foo]\n"
                   "key = value\n"
                   "x = 1\n"
                   "\n"
                   "[bar]\n"
                   "x = y" };

        auto const ok = e.erase_section("foo");

        BOOST_REQUIRE(ok);
        BOOST_TEST(e.section_count() == 1);

        auto const expected = "[bar]\n"
                              "x = y\n";
        BOOST_TEST(e.text() == expected);
    }

    BOOST_AUTO_TEST_CASE(erase_section_3_)
    {
        editor e { "[foo]\n"
                   "key = value\n"
                   "x1 = 1\n"
                   "\n"
                   "x2 = y" };

        auto const ok = e.erase_section("foo");

        BOOST_REQUIRE(ok);
        BOOST_TEST(e.section_count() == 0);
    }

    BOOST_AUTO_TEST_CASE(clear_)
    {
        editor e { "key = value" };

        e.clear();

        BOOST_TEST(e.empty());
    }

    BOOST_AUTO_TEST_CASE(read_)
    {
        editor e { "key = value1" };

        std::istringstream iss { "key = value2" };

        e.read(iss);

        BOOST_TEST(e[""]["key"] == "value2");
        BOOST_TEST(e.text() == "key = value2\n");
    }

    BOOST_AUTO_TEST_CASE(write_)
    {
        editor e { "key = value" };

        std::ostringstream oss;

        e.write(oss);

        BOOST_TEST(oss.str() == "key = value\n");
    }

    BOOST_AUTO_TEST_CASE(load_from_file_)
    {
        temp_file f;
        f << "key = value";

        auto e = load_from_file(f.path());

        BOOST_TEST(e.text() == "key = value\n");
    }

    BOOST_AUTO_TEST_CASE(save_to_file_)
    {
        editor e { "key = value" };
        temp_file f;

        save_to_file(e, f.path());

        std::ifstream ifs { f.path() };
        std::ostringstream oss;

        oss << ifs.rdbuf();

        BOOST_TEST(oss.str() == "key = value\n");
    }

BOOST_AUTO_TEST_SUITE_END() // editor_
