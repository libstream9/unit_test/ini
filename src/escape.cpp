#include <stream9/ini/map/ini_file.hpp>

#include <stream9/ini/error.hpp>
#include <stream9/ini/escape.hpp>

#include <boost/test/unit_test.hpp>

namespace stream9::ini::testing {

BOOST_AUTO_TEST_SUITE(escape_section_)

    BOOST_AUTO_TEST_CASE(empty_)
    {
        auto const& s = escape_section("");

        BOOST_TEST(s == "");
    }

    BOOST_AUTO_TEST_CASE(backslash_)
    {
        auto const& s = escape_section("\\xxx\\yyy\\");

        BOOST_TEST(s == "\\\\xxx\\\\yyy\\\\");
    }

    BOOST_AUTO_TEST_CASE(apostrophe_)
    {
        auto const& s = escape_section("\'xxx\'yyy\'");

        BOOST_TEST(s == "\\'xxx\\'yyy\\'");
    }

    BOOST_AUTO_TEST_CASE(double_quote_)
    {
        auto const& s = escape_section("\"xxx\"yyy\"");

        BOOST_TEST(s == "\\\"xxx\\\"yyy\\\"");
    }

    BOOST_AUTO_TEST_CASE(null_)
    {
        using namespace std::literals;
        auto const& s = escape_section("\0xxx\0yyy\0"sv);

        BOOST_TEST(s == "\\0xxx\\0yyy\\0"sv);
    }

    BOOST_AUTO_TEST_CASE(bell_)
    {
        auto const& s = escape_section("\axxx\ayyy\a");

        BOOST_TEST(s == "\\axxx\\ayyy\\a");
    }

    BOOST_AUTO_TEST_CASE(backspace_)
    {
        auto const& s = escape_section("\bxxx\byyy\b");

        BOOST_TEST(s == "\\bxxx\\byyy\\b");
    }

    BOOST_AUTO_TEST_CASE(tab_)
    {
        auto const& s = escape_section("\txxx\tyyy\t");

        BOOST_TEST(s == "\\txxx\\tyyy\\t");
    }

    BOOST_AUTO_TEST_CASE(carrige_return_)
    {
        auto const& s = escape_section("\rxxx\ryyy\r");

        BOOST_TEST(s == "\\rxxx\\ryyy\\r");
    }

    BOOST_AUTO_TEST_CASE(line_feed_)
    {
        auto const& s = escape_section("\nxxx\nyyy\n");

        BOOST_TEST(s == "\\nxxx\\nyyy\\n");
    }

    BOOST_AUTO_TEST_CASE(semi_colon_)
    {
        auto const& s = escape_section(";xxx;yyy;");

        BOOST_TEST(s == "\";xxx;yyy;\"");
    }

    BOOST_AUTO_TEST_CASE(semi_number_sign_)
    {
        auto const& s = escape_section("#xxx#yyy#");

        BOOST_TEST(s == "\"#xxx#yyy#\"");
    }

    BOOST_AUTO_TEST_CASE(close_bracket_will_be_quoted_)
    {
        auto const& s = escape_section("]xxx]yyy]");

        BOOST_TEST(s == "\"]xxx]yyy]\"");
    }

    BOOST_AUTO_TEST_CASE(leading_trailing_space_will_be_quoted_)
    {
        auto const& s = escape_section("   section   ");

        BOOST_TEST(s == "\"   section   \"");
    }


    BOOST_AUTO_TEST_CASE(space_in_the_middle_wont_be_quoted_)
    {
        auto const& s = escape_section("section one");

        BOOST_TEST(s == "section one");
    }
BOOST_AUTO_TEST_SUITE_END() // escape_section_

BOOST_AUTO_TEST_SUITE(escape_key_)

    BOOST_AUTO_TEST_CASE(empty_)
    {
        auto const& s = escape_key("");

        BOOST_TEST(s == "");
    }

    BOOST_AUTO_TEST_CASE(backslash_)
    {
        auto const& s = escape_key("\\xxx\\yyy\\");

        BOOST_TEST(s == "\\\\xxx\\\\yyy\\\\");
    }

    BOOST_AUTO_TEST_CASE(apostrophe_)
    {
        auto const& s = escape_key("\'xxx\'yyy\'");

        BOOST_TEST(s == "\\'xxx\\'yyy\\'");
    }

    BOOST_AUTO_TEST_CASE(double_quote_)
    {
        auto const& s = escape_key("\"xxx\"yyy\"");

        BOOST_TEST(s == "\\\"xxx\\\"yyy\\\"");
    }

    BOOST_AUTO_TEST_CASE(null_)
    {
        using namespace std::literals;
        auto const& s = escape_key("\0xxx\0yyy\0"sv);

        BOOST_TEST(s == "\\0xxx\\0yyy\\0"sv);
    }

    BOOST_AUTO_TEST_CASE(bell_)
    {
        auto const& s = escape_key("\axxx\ayyy\a");

        BOOST_TEST(s == "\\axxx\\ayyy\\a");
    }

    BOOST_AUTO_TEST_CASE(backspace_)
    {
        auto const& s = escape_key("\bxxx\byyy\b");

        BOOST_TEST(s == "\\bxxx\\byyy\\b");
    }

    BOOST_AUTO_TEST_CASE(tab_)
    {
        auto const& s = escape_key("\txxx\tyyy\t");

        BOOST_TEST(s == "\\txxx\\tyyy\\t");
    }

    BOOST_AUTO_TEST_CASE(carrige_return_)
    {
        auto const& s = escape_key("\rxxx\ryyy\r");

        BOOST_TEST(s == "\\rxxx\\ryyy\\r");
    }

    BOOST_AUTO_TEST_CASE(line_feed_)
    {
        auto const& s = escape_key("\nxxx\nyyy\n");

        BOOST_TEST(s == "\\nxxx\\nyyy\\n");
    }

    BOOST_AUTO_TEST_CASE(semi_colon_)
    {
        auto const& s = escape_key(";xxx;yyy;");

        BOOST_TEST(s == "\\;xxx\\;yyy\\;");
    }

    BOOST_AUTO_TEST_CASE(semi_number_sign_)
    {
        auto const& s = escape_key("#xxx#yyy#");

        BOOST_TEST(s == "\\#xxx\\#yyy\\#");
    }

    BOOST_AUTO_TEST_CASE(equal_)
    {
        auto const& s = escape_key("=xxx=yyy=");

        BOOST_TEST(s == "\\=xxx\\=yyy\\=");
    }

    BOOST_AUTO_TEST_CASE(open_bracket_)
    {
        auto const& s = escape_key("[xxx[yyy[");

        BOOST_TEST(s == "\\[xxx[yyy[");
    }

BOOST_AUTO_TEST_SUITE_END() // escape_key_

BOOST_AUTO_TEST_SUITE(escape_value_)

    BOOST_AUTO_TEST_CASE(empty_)
    {
        auto const& s = escape_value("");

        BOOST_TEST(s == "");
    }

    BOOST_AUTO_TEST_CASE(backslash_)
    {
        auto const& s = escape_value("\\xxx\\yyy\\");

        BOOST_TEST(s == "\\\\xxx\\\\yyy\\\\");
    }

    BOOST_AUTO_TEST_CASE(apostrophe_)
    {
        auto const& s = escape_value("\'xxx\'yyy\'");

        BOOST_TEST(s == "\\'xxx\\'yyy\\'");
    }

    BOOST_AUTO_TEST_CASE(double_quote_)
    {
        auto const& s = escape_value("\"xxx\"yyy\"");

        BOOST_TEST(s == "\\\"xxx\\\"yyy\\\"");
    }

    BOOST_AUTO_TEST_CASE(null_)
    {
        using namespace std::literals;
        auto const& s = escape_value("\0xxx\0yyy\0"sv);

        BOOST_TEST(s == "\\0xxx\\0yyy\\0"sv);
    }

    BOOST_AUTO_TEST_CASE(bell_)
    {
        auto const& s = escape_value("\axxx\ayyy\a");

        BOOST_TEST(s == "\\axxx\\ayyy\\a");
    }

    BOOST_AUTO_TEST_CASE(backspace_)
    {
        auto const& s = escape_value("\bxxx\byyy\b");

        BOOST_TEST(s == "\\bxxx\\byyy\\b");
    }

    BOOST_AUTO_TEST_CASE(tab_)
    {
        auto const& s = escape_value("\txxx\tyyy\t");

        BOOST_TEST(s == "\\txxx\\tyyy\\t");
    }

    BOOST_AUTO_TEST_CASE(carrige_return_)
    {
        auto const& s = escape_value("\rxxx\ryyy\r");

        BOOST_TEST(s == "\\rxxx\\ryyy\\r");
    }

    BOOST_AUTO_TEST_CASE(line_feed_)
    {
        auto const& s = escape_value("\nxxx\nyyy\n");

        BOOST_TEST(s == "\\nxxx\\nyyy\\n");
    }

    BOOST_AUTO_TEST_CASE(semi_colon_)
    {
        auto const& s = escape_value(";xxx;yyy;");

        BOOST_TEST(s == "\\;xxx\\;yyy\\;");
    }

    BOOST_AUTO_TEST_CASE(semi_number_sign_)
    {
        auto const& s = escape_value("#xxx#yyy#");

        BOOST_TEST(s == "\\#xxx\\#yyy\\#");
    }

BOOST_AUTO_TEST_SUITE_END() // escape_value_

BOOST_AUTO_TEST_SUITE(unescape_)

    BOOST_AUTO_TEST_CASE(empty_)
    {
        auto const& s = unescape("");

        BOOST_TEST(s == "");
    }

    BOOST_AUTO_TEST_CASE(backslash_)
    {
        auto const& s = unescape("\\\\xxx\\\\yyy\\\\");

        BOOST_TEST(s == "\\xxx\\yyy\\");
    }

    BOOST_AUTO_TEST_CASE(apostrophe_)
    {
        auto const& s = unescape("\\'xxx\\'yyy\\'");

        BOOST_TEST(s == "\'xxx\'yyy\'");
    }

    BOOST_AUTO_TEST_CASE(double_quote_)
    {
        auto const& s = unescape("\\\"xxx\\\"yyy\\\"");

        BOOST_TEST(s == "\"xxx\"yyy\"");
    }

    BOOST_AUTO_TEST_CASE(null_)
    {
        using namespace std::literals;
        auto const& s = unescape("\\0xxx\\0yyy\\0"sv);

        BOOST_TEST(s == "\0xxx\0yyy\0"sv);
    }

    BOOST_AUTO_TEST_CASE(bell_)
    {
        auto const& s = unescape("\\axxx\\ayyy\\a");

        BOOST_TEST(s == "\axxx\ayyy\a");
    }

    BOOST_AUTO_TEST_CASE(backspace_)
    {
        auto const& s = unescape("\\bxxx\\byyy\\b");

        BOOST_TEST(s == "\bxxx\byyy\b");
    }

    BOOST_AUTO_TEST_CASE(tab_)
    {
        auto const& s = unescape("\\txxx\\tyyy\\t");

        BOOST_TEST(s == "\txxx\tyyy\t");
    }

    BOOST_AUTO_TEST_CASE(carrige_return_)
    {
        auto const& s = unescape("\\rxxx\\ryyy\\r");

        BOOST_TEST(s == "\rxxx\ryyy\r");
    }

    BOOST_AUTO_TEST_CASE(line_feed_)
    {
        auto const& s = unescape("\\nxxx\\nyyy\\n");

        BOOST_TEST(s == "\nxxx\nyyy\n");
    }

    BOOST_AUTO_TEST_CASE(semi_colon_)
    {
        auto const& s = unescape("\\;xxx\\;yyy\\;");

        BOOST_TEST(s == ";xxx;yyy;");
    }

    BOOST_AUTO_TEST_CASE(number_sign_)
    {
        auto const& s = unescape("\\#xxx\\#yyy\\#");

        BOOST_TEST(s == "#xxx#yyy#");
    }

    BOOST_AUTO_TEST_CASE(equal_)
    {
        auto const& s = unescape("\\=xxx\\=yyy\\=");

        BOOST_TEST(s == "=xxx=yyy=");
    }

    BOOST_AUTO_TEST_CASE(open_bracket_)
    {
        auto const& s = unescape("\\[xxx\\[yyy\\[");

        BOOST_TEST(s == "[xxx[yyy[");
    }

    BOOST_AUTO_TEST_CASE(close_bracket_)
    {
        auto const& s = unescape("\\]xxx\\]yyy\\]");

        BOOST_TEST(s == "]xxx]yyy]");
    }

    BOOST_AUTO_TEST_CASE(unicode_)
    {
        auto const& s = unescape("\\x0001xxx\\x0002yyy\\x0003");

        BOOST_TEST(s == "\x01xxx\x02yyy\x03");
    }

    BOOST_AUTO_TEST_CASE(invalid_escape_seq_1_)
    {
        auto const text = "xxx\\";

        BOOST_CHECK_EXCEPTION(
            unescape(text),
            unknown_escape_sequence,
            [&](auto& e) {
                auto const pos = e.range().begin() - text;
                BOOST_TEST(pos == 3);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(invalid_escape_seq_2_)
    {
        auto const text = "xxx\\p";

        BOOST_CHECK_EXCEPTION(
            unescape(text),
            unknown_escape_sequence,
            [&](auto& e) {
                auto const pos = e.range().begin() - text;
                BOOST_TEST(pos == 3);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(invalid_utf16_seq_1_)
    {
        auto const text = "\\x0";

        BOOST_CHECK_EXCEPTION(
            unescape(text),
            unknown_escape_sequence,
            [&](auto& e) {
                auto const pos = e.range().begin() - text;
                BOOST_TEST(pos == 0);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(invalid_utf16_seq_2_)
    {
        auto const text = "\\x010G";

        BOOST_CHECK_EXCEPTION(
            unescape(text),
            unknown_escape_sequence,
            [&](auto& e) {
                auto const pos = e.range().begin() - text;
                BOOST_TEST(pos == 0);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(invalid_utf16_seq_3_)
    {
        auto const text = "\\xYYYY";

        BOOST_CHECK_EXCEPTION(
            unescape(text),
            unknown_escape_sequence,
            [&](auto& e) {
                auto const pos = e.range().begin() - text;
                BOOST_TEST(pos == 0);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(invalid_utf16_seq_4_)
    {
        auto const text = "\\xDC00"; // surrogate pair

        BOOST_CHECK_EXCEPTION(
            unescape(text),
            unknown_escape_sequence,
            [&](auto& e) {
                auto const pos = e.range().begin() - text;
                BOOST_TEST(pos == 0);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(continuation_)
    {
        auto const& s = unescape_continuation("\\\nline1\\\nline2\\\n");

        BOOST_TEST(s == "line1line2");
    }

    BOOST_AUTO_TEST_CASE(unquote_1_)
    {
        auto const text = R"("quoted")";

        BOOST_TEST(unquote(text) == "quoted");
    }

    BOOST_AUTO_TEST_CASE(unquote_2_)
    {
        auto const text = R"(quoted)";

        BOOST_TEST(unquote(text) == "quoted");
    }

    BOOST_AUTO_TEST_CASE(unquote_unclosed_quote_1_)
    {
        auto const text = R"('quoted)";

        BOOST_TEST(unquote(text) == "'quoted");
    }

    BOOST_AUTO_TEST_CASE(unquote_unclosed_quote_2_)
    {
        auto const text = R"(')";

        BOOST_TEST(unquote(text) == "'");
    }

BOOST_AUTO_TEST_SUITE_END() // unescape_

} // namespace stream9::ini::testing

