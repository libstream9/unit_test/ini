#include "event_recorder.hpp"

#include <stream9/ini/scanner.hpp>

#include <boost/test/unit_test.hpp>

namespace stream9::ini::testing {

BOOST_AUTO_TEST_SUITE(scanner_)

BOOST_AUTO_TEST_SUITE(misc_)

    BOOST_AUTO_TEST_CASE(empty_)
    {
        auto const text = "";

        event_recorder r;

        ini::scan(text, r);

        auto const expected = "";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(blank_line_1_)
    {
        auto const text = "\n";

        event_recorder r;

        ini::scan(text, r);

        auto const expected = "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(blank_line_2_)
    {
        auto const text = "\n\n";

        event_recorder r;

        ini::scan(text, r);

        auto const expected = "new_line\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(cr_)
    {
        auto const text = "\r";

        event_recorder r;

        ini::scan(text, r);

        auto const expected = "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(crlf_)
    {
        auto const text = "\r\n";

        event_recorder r;

        ini::scan(text, r);

        auto const expected = "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(default_handler_)
    {
        auto const text = "[section] ; comment\n"
                          "key = value\n"
                          "error";

        event_handler h;

        BOOST_CHECK_THROW(
            ini::scan(text, h),
            scan_error );
    }

BOOST_AUTO_TEST_SUITE_END() // misc_

BOOST_AUTO_TEST_SUITE_END() // scanner_

} // namespace stream9::ini::testing
