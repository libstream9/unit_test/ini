#include "event_recorder.hpp"

#include <stream9/ini/scanner.hpp>

#include <boost/test/unit_test.hpp>

namespace stream9::ini::testing {

BOOST_AUTO_TEST_SUITE(scanner_)

BOOST_AUTO_TEST_SUITE(key_)

    BOOST_AUTO_TEST_CASE(empty_)
    {
        auto const text = " = value";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: empty key, pos = 1\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(with_space_)
    {
        auto const text = "   key key\t  =value\n";

        event_recorder r;

        ini::scan(text, r);

        auto const expected = "entry: key key = value\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(invalid_char_1_)
    {
        auto const text = "key; = value";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: key value separator is expected, pos = 3\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(invalid_char_2_)
    {
        auto const text = "key\n = value";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: key value separator is expected, pos = 3\n"
                              "new_line\n"
                              "error: empty key, pos = 5\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(invalid_char_3_)
    {
        auto const text = "key\b = value";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: invalid character, pos = 3\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(escape_seq_)
    {
        auto const text = "\\;xxx\\=yyy\\n = value";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "entry: \\;xxx\\=yyy\\n = value\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(quoted_)
    {
        auto const text = "\"key\" = value";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "entry: \"key\" = value\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(quoted_empty_)
    {
        auto const text = "\"\" = value";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: empty key, pos = 0\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(quoted_with_space_)
    {
        auto const text = "\' \t key \t \' = value";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "entry: \' \t key \t \' = value\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(quoted_with_reserve_char_)
    {
        auto const text = "\"key[one=#1;]\" = value";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "entry: \"key[one=#1;]\" = value\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(quoted_with_escape_seq_)
    {
        auto const text = "\'\\bxxx\\byyy\\b\' = value";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "entry: \'\\bxxx\\byyy\\b\' = value\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(unclosed_quote_1_)
    {
        auto const text = "\"";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: unclosed quote, pos = 0\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(unclosed_quote_2_)
    {
        auto const text = "\"key = value\n";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: unclosed quote, pos = 0\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(unmatched_quote_)
    {
        auto const text = "\'name\"\n";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: unclosed quote, pos = 0\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(quoted_with_invalid_char_)
    {
        auto const text = "\"\t\bname\b\"\n";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: invalid character, pos = 2\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(unquoted_with_quote_)
    {
        auto const text = "name \'one\' = value";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "entry: name \'one\' = value\n";

        BOOST_TEST(r.str() == expected);
    }

BOOST_AUTO_TEST_SUITE_END() // key_

BOOST_AUTO_TEST_SUITE_END() // scanner_

} // namespace stream9::ini::testing
