#include "event_recorder.hpp"

#include <stream9/ini/scanner.hpp>

#include <boost/test/unit_test.hpp>

namespace stream9::ini::testing {

BOOST_AUTO_TEST_SUITE(scanner_)

BOOST_AUTO_TEST_SUITE(section_)

    BOOST_AUTO_TEST_CASE(no_entry_1_)
    {
        auto const text = "[section]";

        event_recorder r;

        ini::scan(text, r);

        auto const expected = "section: section\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(no_entry_2_)
    {
        auto const text = "[section]   \n";

        event_recorder r;

        ini::scan(text, r);

        auto const expected = "section: section\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(one_entry_)
    {
        auto const text = "[section]\n"
                          "name = value\n";

        event_recorder r;

        ini::scan(text, r);

        auto const expected = "section: section\n"
                              "new_line\n"
                              "entry: name = value\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(with_blank_line_)
    {
        auto const text = "[section]\n"
                          "\n"
                          "name = value\n";

        event_recorder r;

        ini::scan(text, r);

        auto const expected = "section: section\n"
                              "new_line\n"
                              "new_line\n"
                              "entry: name = value\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(multiple_entry_)
    {
        auto const text = "[section]\n"
                          "name1 = value1\n"
                          "name2 = value2\n";

        event_recorder r;

        ini::scan(text, r);

        auto const expected = "section: section\n"
                              "new_line\n"
                              "entry: name1 = value1\n"
                              "new_line\n"
                              "entry: name2 = value2\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(with_comment_1_)
    {
        auto const text = "[section] ; comment\n"
                          "name1 = value1\n";

        event_recorder r;

        ini::scan(text, r);

        auto const expected = "section: section\n"
                              "comment: ; comment\n"
                              "new_line\n"
                              "entry: name1 = value1\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(with_comment_2_)
    {
        auto const text = "[section] # comment\n"
                          "name1 = value1\n";

        event_recorder r;

        ini::scan(text, r);

        auto const expected = "section: section\n"
                              "comment: # comment\n"
                              "new_line\n"
                              "entry: name1 = value1\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(with_comment_3_)
    {
        auto const text = "[section]\n"
                          "; comment\n"
                          "name1 = value1\n";

        event_recorder r;

        ini::scan(text, r);

        auto const expected = "section: section\n"
                              "new_line\n"
                              "comment: ; comment\n"
                              "new_line\n"
                              "entry: name1 = value1\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(multiple_section_)
    {
        auto const text = "[section1]\n"
                          "name1 = value1\n"
                          "[section2]\n"
                          "name2 = value2\n";

        event_recorder r;

        ini::scan(text, r);

        auto const expected = "section: section1\n"
                              "new_line\n"
                              "entry: name1 = value1\n"
                              "new_line\n"
                              "section: section2\n"
                              "new_line\n"
                              "entry: name2 = value2\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(leading_space_)
    {
        auto const text = "  [section]\n"
                          "  name = value";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "section: section\n"
                              "new_line\n"
                              "entry: name = value\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(empty_section_name_)
    {
        auto const text = "[]\n"
                          "name = value";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: section name is expected, pos = 1\n"
                              "new_line\n"
                              "entry: name = value\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(section_name_with_leading_trailing_space_)
    {
        auto const text = "[  section  ]\n"
                          "name = value";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "section: section\n"
                              "new_line\n"
                              "entry: name = value\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(invalid_section_name_1_)
    {
        auto const text = "[section;]\n"
                          "name = value";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: invalid section name, pos = 8\n"
                              "new_line\n"
                              "entry: name = value\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(invalid_section_name_2_)
    {
        auto const text = "[section\b]\n"
                          "name = value";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: invalid section name, pos = 8\n"
                              "new_line\n"
                              "entry: name = value\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(invalid_section_name_3_)
    {
        auto const text = "[section\n"
                          "name = value";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: section terminator is expected, pos = 8\n"
                              "new_line\n"
                              "entry: name = value\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(no_new_line_after_header_)
    {
        auto const text = "[section] name1 = value1\n"
                          "name2 = value2";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "section: section\n"
                              "error: new line is expected, pos = 10\n"
                              "new_line\n"
                              "entry: name2 = value2\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(name_with_escape_sequence_)
    {
        auto const text = "[\\axxx\\byyy\\t]\n";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "section: \\axxx\\byyy\\t\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(quoted_name_)
    {
        auto const text = "[\"name\"]\n";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "section: \"name\"\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(quoted_name_with_space_)
    {
        auto const text = "[\'  name  \']\n";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "section: \'  name  \'\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(quoted_name_with_tab_)
    {
        auto const text = "[\"\tname\tname\t\"]\n";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "section: \"\tname\tname\t\"\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(quoted_name_with_reserve_char_)
    {
        auto const text = "[\';xxx[yyy]\']\n";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "section: \';xxx[yyy]\'\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(quoted_name_with_escape_seq_)
    {
        auto const text = "[\"\\bxxx\\byyy\\b\"]\n";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "section: \"\\bxxx\\byyy\\b\"\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(unclosed_quote_)
    {
        auto const text = "[\'name]\n";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: unclosed quote, pos = 1\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(unmatched_quote_)
    {
        auto const text = "[\"name\']\n";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: unclosed quote, pos = 1\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(quoted_name_with_invalid_char_)
    {
        auto const text = "[\'\t\bname\b\']\n";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: invalid section name, pos = 3\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(quoted_empty_name_)
    {
        auto const text = "[\"\"]\n";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: section name is expected, pos = 1\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(unquoted_name_with_quote_)
    {
        auto const text = "[name \"one\"]\n";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "section: name \"one\"\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

BOOST_AUTO_TEST_SUITE_END() // section_

BOOST_AUTO_TEST_SUITE_END() // scanner_

} // namespace stream9::ini::testing
