#include "event_recorder.hpp"

#include <stream9/ini/scanner.hpp>

#include <boost/test/unit_test.hpp>

namespace stream9::ini::testing {

BOOST_AUTO_TEST_SUITE(scanner_)

BOOST_AUTO_TEST_SUITE(value_)

    BOOST_AUTO_TEST_CASE(empty_)
    {
        auto const text = "key =";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "entry: key = \n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(with_space_)
    {
        auto const text = "key =  \t  value \t 1  \t";

        event_recorder r;

        ini::scan(text, r);

        auto const expected = "entry: key = value \t 1\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(multi_line_value_)
    {
        auto const text = "key1 =   value1\\\n"
                          "       , value2\n";

        event_recorder r;

        ini::scan(text, r);

        auto const expected = "entry: key1 = value1\\\n       , value2\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(invalid_char_1_)
    {
        auto const text = "key = #value";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "entry: key = \n"
                              "comment: #value\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(invalid_char_2_)
    {
        auto const text = "key = value\b";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: invalid character, pos = 11\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(escape_seq_)
    {
        auto const text = "key = \\\\ \\\' \\\" \\0 \\a \\b \\t \\r \\n \\; \\# \\= \\[ \\]";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "entry: key = \\\\ \\\' \\\" \\0 \\a \\b \\t \\r \\n \\; \\# \\= \\[ \\]\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(invalid_escape_seq_1_)
    {
        auto const text = "key = value\\";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: unknown escape sequence, pos = 11\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(invalid_escape_seq_2_)
    {
        auto const text = "key = \\p";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: unknown escape sequence, pos = 6\n";

        BOOST_TEST(r.str() == expected);
    }


    BOOST_AUTO_TEST_CASE(quoted_)
    {
        auto const text = "key = 'value'";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "entry: key = 'value'\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(quoted_empty)
    {
        auto const text = "key = \"\"";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "entry: key = \"\"\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(quoted_with_space_)
    {
        auto const text = "key = \' \t value \t \'";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "entry: key = \' \t value \t \'\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(quoted_with_reserve_char_)
    {
        auto const text = "key = \"value[one=#1;]\"";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "entry: key = \"value[one=#1;]\"\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(quoted_with_escape_seq_)
    {
        auto const text = "key = \'\\bxxx\\byyy\\b\'";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "entry: key = \'\\bxxx\\byyy\\b\'\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(unquoted_with_quote_)
    {
        auto const text = "key = value \'one\'";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "entry: key = value \'one\'\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(unclosed_quote_1_)
    {
        auto const text = "key = \"value";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: unclosed quote, pos = 6\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(unclosed_quote_2_)
    {
        auto const text = "key = ' value ; comment";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: unclosed quote, pos = 6\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(unclosed_quote_3_)
    {
        auto const text = "key = \"";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: unclosed quote, pos = 6\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(unmatched_quote_)
    {
        auto const text = "key = \"value\'";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: unclosed quote, pos = 6\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(quoted_with_invalid_char_)
    {
        auto const text = "key = \"\t\bvalue\b\"";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: invalid character, pos = 8\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(unicode_seq_)
    {
        auto const text = "key = value\\x0030";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "entry: key = value\\x0030\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(invalid_unicode_seq_1_)
    {
        auto const text = "key = value\\x";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: unknown escape sequence, pos = 11\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(invalid_unicode_seq_2_)
    {
        auto const text = "key = value\\x1";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: unknown escape sequence, pos = 11\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(invalid_unicode_seq_3_)
    {
        auto const text = "key = value\\x12";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: unknown escape sequence, pos = 11\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(invalid_unicode_seq_4_)
    {
        auto const text = "key = value\\x123";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: unknown escape sequence, pos = 11\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(invalid_unicode_seq_5_)
    {
        auto const text = "key = value\\xX234";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: unknown escape sequence, pos = 11\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(invalid_unicode_seq_6_)
    {
        auto const text = "key = value\\x1X34";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: unknown escape sequence, pos = 11\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(invalid_unicode_seq_7_)
    {
        auto const text = "key = value\\x12X4";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: unknown escape sequence, pos = 11\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(invalid_unicode_seq_8_)
    {
        auto const text = "key = value\\x123X";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: unknown escape sequence, pos = 11\n";

        BOOST_TEST(r.str() == expected);
    }

BOOST_AUTO_TEST_SUITE_END() // value_

BOOST_AUTO_TEST_SUITE_END() // scanner_

} // namespace stream9::ini::testing
