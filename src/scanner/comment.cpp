#include "event_recorder.hpp"

#include <stream9/ini/scanner.hpp>

#include <boost/test/unit_test.hpp>

namespace stream9::ini::testing {

BOOST_AUTO_TEST_SUITE(scanner_)

BOOST_AUTO_TEST_SUITE(misc_)

    BOOST_AUTO_TEST_CASE(one_comment_1_)
    {
        auto const text = "; comment";

        event_recorder r;

        ini::scan(text, r);

        auto const expected = "comment: ; comment\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(one_comment_2_)
    {
        auto const text = "# comment\n";

        event_recorder r;

        ini::scan(text, r);

        auto const expected = "comment: # comment\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(multiple_comment_)
    {
        auto const text = "; comment1\n"
                          "# comment2";

        event_recorder r;

        ini::scan(text, r);

        auto const expected = "comment: ; comment1\n"
                              "new_line\n"
                              "comment: # comment2\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(multi_line_)
    {
        auto const text = "; comment1 \\\n"
                          "# comment2\n"
                          "; comment3";

        event_recorder r;

        ini::scan(text, r);

        auto const expected = "comment: ; comment1 \\\n# comment2\n"
                              "new_line\n"
                              "comment: ; comment3\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(leading_space_)
    {
        auto const text = "    ; comment";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "comment: ; comment\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(invalid_char_)
    {
        auto const text = "# \acomment";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: invalid character, pos = 2\n";

        BOOST_TEST(r.str() == expected);
    }

BOOST_AUTO_TEST_SUITE_END() // misc_

BOOST_AUTO_TEST_SUITE_END() // scanner_

} // namespace stream9::ini::testing
