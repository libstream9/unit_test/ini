#ifndef STREAM9_INI_TEST_SCANNER_EVENT_RECORDER_HPP
#define STREAM9_INI_TEST_SCANNER_EVENT_RECORDER_HPP

#include <stream9/ini/scanner.hpp>

#include <sstream>
#include <string>
#include <string_view>

namespace stream9::ini::testing {

class event_recorder : public ini::event_handler
{
public:
    event_recorder(std::string_view text = {})
        : m_text { text }
    {}

    std::string str() const
    {
        return m_oss.str();
    }

    void clear()
    {
        m_oss.str("");
    }

    // override ini::event_handler
    void on_section(std::string_view name) override
    {
        m_oss << "section: " << name << '\n';
    }

    void on_entry(std::string_view const key, std::string_view const value) override
    {
        m_oss << "entry: " << key << " = " << value << "\n";
    }

    void on_comment(std::string_view const content) override
    {
        m_oss << "comment: " << content << "\n";
    }

    void on_newline(std::string_view /*content*/) override
    {
        m_oss << "new_line\n";
    }

    void on_error(error_code const code, std::string_view const content) override
    {
        m_oss << "error: " << ini::to_string(code) << ", ";

        if (m_text.empty()) {
            m_oss << "content = " << content << "\n";
        }
        else {
            auto const pos = content.begin() - m_text.begin();
            m_oss << "pos = " << pos << "\n";
        }
    }

private:
    std::string_view m_text;
    std::ostringstream m_oss;
};

} // namespace stream9::ini::testing

#endif // STREAM9_INI_TEST_SCANNER_EVENT_RECORDER_HPP
