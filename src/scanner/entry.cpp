#include "event_recorder.hpp"

#include <stream9/ini/scanner.hpp>

#include <boost/test/unit_test.hpp>

namespace stream9::ini::testing {

BOOST_AUTO_TEST_SUITE(scanner_)

BOOST_AUTO_TEST_SUITE(entry_)

    BOOST_AUTO_TEST_CASE(single_entry_without_new_line_)
    {
        auto const text = "key=value";

        event_recorder r;

        ini::scan(text, r);

        auto const expected = "entry: key = value\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(single_entry_with_new_line_)
    {
        auto const text = "key=value\n";

        event_recorder r;

        ini::scan(text, r);

        auto const expected = "entry: key = value\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(multiple_entry_)
    {
        auto const text = "key1=value1\n"
                          "key2  =   value2  \n";

        event_recorder r;

        ini::scan(text, r);

        auto const expected = "entry: key1 = value1\n"
                              "new_line\n"
                              "entry: key2 = value2\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(with_comment_1_)
    {
        auto const text = "key = value  ; comment";

        event_recorder r;

        ini::scan(text, r);

        auto const expected = "entry: key = value\n"
                              "comment: ; comment\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(with_comment_2_)
    {
        auto const text = "key = value  # comment  ";

        event_recorder r;

        ini::scan(text, r);

        auto const expected = "entry: key = value\n"
                              "comment: # comment  \n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(key_only_1_)
    {
        auto const text = "key";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: key value separator is expected, pos = 3\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(key_only_2_)
    {
        auto const text = "key\n";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "error: key value separator is expected, pos = 3\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

    BOOST_AUTO_TEST_CASE(empty_value_with_comment_)
    {
        auto const text = "key = # empty\n";

        event_recorder r { text };

        ini::scan(text, r);

        auto const expected = "entry: key = \n"
                              "comment: # empty\n"
                              "new_line\n";

        BOOST_TEST(r.str() == expected);
    }

BOOST_AUTO_TEST_SUITE_END() // entry_

BOOST_AUTO_TEST_SUITE_END() // scanner_

} // namespace stream9::ini::testing
