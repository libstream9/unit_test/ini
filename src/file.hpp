#ifndef STREAM9_INI_TEST_FILE_HPP
#define STREAM9_INI_TEST_FILE_HPP

#include <filesystem>
#include <string_view>

#include <stdio.h>

namespace stream9::ini {

class temp_file
{
public:
    using path_t = std::filesystem::path;

public:
    temp_file()
        : m_path { "ini_test_XXXXXX" }
        , m_stream { ::fdopen(::mkstemp(m_path.data()), "w") }
    {}

    ~temp_file()
    {
        ::fclose(m_stream);
        ::remove(m_path.c_str());
    }

    path_t path() const { return m_path.c_str(); }

    temp_file& operator<<(std::string_view const s)
    {
        for (auto const c: s) {
            ::fputc(c, m_stream);
        }
        ::fflush(m_stream);

        return *this;
    }

private:
    std::string m_path;
    ::FILE* m_stream;
};

} // namespace stream9::ini

#endif // STREAM9_INI_TEST_FILE_HPP
