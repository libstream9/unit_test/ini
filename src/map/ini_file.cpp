#include <stream9/ini/map/ini_file.hpp>

#include "../json.hpp"
#include "../file.hpp"

#include <fstream>
#include <sstream>

#include <boost/test/unit_test.hpp>

namespace stream9::ini::testing {

BOOST_AUTO_TEST_SUITE(ini_file_)

BOOST_AUTO_TEST_CASE(basic_)
{
    temp_file f;
    f << "foo = bar";

    ini_file ini { f.path() };

    BOOST_TEST(ini.size() == 1);

    auto const& s = ini.default_section();
    BOOST_TEST(s.size() == 1);

    auto* const v = s.find("foo");
    BOOST_REQUIRE(v);
    BOOST_TEST(*v == "bar");
}

BOOST_AUTO_TEST_CASE(find_section_success_)
{
    temp_file f;
    f << "[section]\n"
      << "key1 = value1\n";

    ini_file ini { f.path() };

    auto* const s = ini.find_section("section");
    BOOST_TEST(s);
}

BOOST_AUTO_TEST_CASE(find_section_fail_)
{
    temp_file f;
    f << "[section]\n"
      << "key1 = value1\n";

    ini_file ini { f.path() };

    auto* const s = ini.find_section("xxx");
    BOOST_TEST(!s);
}

BOOST_AUTO_TEST_CASE(find_section_const_success_)
{
    temp_file f;
    f << "[section]\n"
      << "key1 = value1\n";

    ini_file const ini { f.path() };

    auto* const s = ini.find_section("section");
    BOOST_TEST(s);
}

BOOST_AUTO_TEST_CASE(find_section_const_fail_)
{
    temp_file f;
    f << "[section]\n"
      << "key1 = value1\n";

    ini_file const ini { f.path() };

    auto* const s = ini.find_section("xxx");
    BOOST_TEST(!s);
}

BOOST_AUTO_TEST_CASE(default_section_)
{
    temp_file f;
    f << "[section]\n"
      << "key1 = value1\n";

    ini_file ini { f.path() };

    auto* const s = ini.find_section("section");
    BOOST_TEST(s);

    auto& def = ini.default_section();
    BOOST_TEST(&def != s);
}

BOOST_AUTO_TEST_CASE(default_section_const_)
{
    temp_file f;
    f << "[section]\n"
      << "key1 = value1\n";

    ini_file const ini { f.path() };

    auto* const s = ini.find_section("section");
    BOOST_TEST(s);

    auto& def = ini.default_section();
    BOOST_TEST(&def != s);
}

BOOST_AUTO_TEST_CASE(contains_success_)
{
    temp_file f;
    f << "[section]\n"
      << "key1 = value1\n";

    ini_file ini { f.path() };

    BOOST_TEST(ini.contains("section"));
}

BOOST_AUTO_TEST_CASE(contains_fail_)
{
    temp_file f;
    f << "[section]\n"
      << "key1 = value1\n";

    ini_file const ini { f.path() };

    BOOST_TEST(!ini.contains("xxx"));
}

BOOST_AUTO_TEST_CASE(size_0_)
{
    temp_file f;

    ini_file const ini { f.path() };

    BOOST_TEST(ini.size() == 1);
}

BOOST_AUTO_TEST_CASE(size_1_)
{
    temp_file f;
    f << "[section]\n"
      << "key1 = value1\n";

    ini_file const ini { f.path() };

    BOOST_TEST(ini.size() == 2);
}

BOOST_AUTO_TEST_CASE(size_2_)
{
    temp_file f;
    f << "[section]\n"
      << "key1 = value1\n"
      << "[second]\n"
      << "key2 = value2\n";

    ini_file const ini { f.path() };

    BOOST_TEST(ini.size() == 3);
}

BOOST_AUTO_TEST_CASE(create_section_success_)
{
    temp_file f;

    ini_file ini { f.path() };

    BOOST_REQUIRE_NO_THROW(ini.create_section("foo"));
    BOOST_TEST(ini.size() == 2);

    auto* const s = ini.find_section("foo");
    BOOST_TEST(s);
}

BOOST_AUTO_TEST_CASE(create_section_fail_1_)
{
    temp_file f;
    f << "[section]\n"
      << "key1 = value1\n";

    ini_file ini { f.path() };

    BOOST_CHECK_EXCEPTION(
        ini.create_section("section"),
        ini::error,
        [&](auto& e) {
            return e.code() == error_code::duplicate_section_name;
        } );
}

BOOST_AUTO_TEST_CASE(create_section_fail_2_)
{
    temp_file f;

    ini_file ini { f.path() };

    BOOST_CHECK_EXCEPTION(
        ini.create_section(""),
        ini::error,
        [&](auto& e) {
            return e.code() == error_code::duplicate_section_name;
        } );
}

BOOST_AUTO_TEST_CASE(find_or_create_section_)
{
    temp_file f;
    f << "[foo]\n"
      << "key1 = value1\n";

    ini_file ini { f.path() };

    BOOST_TEST(ini.size() == 2);

    ini.find_or_create_section("foo");

    BOOST_TEST(ini.size() == 2);

    ini.find_or_create_section("bar");

    BOOST_TEST(ini.size() == 3);
}

BOOST_AUTO_TEST_CASE(erase_section_success_)
{
    temp_file f;
    f << "[foo]\n"
      << "key1 = value1\n";

    ini_file ini { f.path() };

    BOOST_TEST(ini.size() == 2);

    auto const ok = ini.erase_section("foo");

    BOOST_REQUIRE(ok);
    BOOST_TEST(ini.size() == 1);
}

BOOST_AUTO_TEST_CASE(erase_section_fail_1_)
{
    temp_file f;
    f << "[foo]\n"
      << "key1 = value1\n";

    ini_file ini { f.path() };

    BOOST_TEST(ini.size() == 2);

    auto const ok = ini.erase_section("bar");

    BOOST_REQUIRE(!ok);
    BOOST_TEST(ini.size() == 2);
}

BOOST_AUTO_TEST_CASE(erase_section_fail_2_)
{
    temp_file f;

    ini_file ini { f.path() };

    BOOST_TEST(ini.size() == 1);

    auto const ok = ini.erase_section("");

    BOOST_REQUIRE(!ok);
    BOOST_TEST(ini.size() == 1);
}

BOOST_AUTO_TEST_CASE(clear_)
{
    temp_file f;
    f << "[foo]\n"
      << "key1 = value1\n";

    ini_file ini { f.path() };

    BOOST_TEST(ini.size() == 2);

    ini.clear();

    BOOST_TEST(ini.size() == 1);
}

BOOST_AUTO_TEST_CASE(save_)
{
    temp_file f;

    ini_file ini { f.path() };

    ini["foo"]["key1"] = "value1";
    ini.save();

    std::ifstream ifs { f.path().c_str() };
    BOOST_TEST(ifs.is_open());

    std::ostringstream oss;
    oss << ifs.rdbuf();

    auto const expected = "[foo]\n"
                          "key1 = value1\n";
    BOOST_TEST(oss.str() == expected);
}

BOOST_AUTO_TEST_CASE(reload_)
{
    temp_file f;
    f << "[foo]\n"
      << "key1 = value1\n";

    ini_file ini { f.path() };

    BOOST_TEST(ini.size() == 2);

    ini["bar"]["key"] = "value";

    BOOST_TEST(ini.size() == 3);

    ini.reload();

    BOOST_TEST(ini.size() == 2);
}

BOOST_AUTO_TEST_CASE(iterator_)
{
    temp_file f;
    f << "[foo]\n"
      << "key1 = value1\n";

    ini_file ini { f.path() };

    auto it = ini.begin();
    auto const end = ini.end();

    BOOST_REQUIRE((it != end));
    BOOST_TEST(it->name() == "");

    ++it;
    BOOST_REQUIRE((it != end));
    BOOST_TEST(it->name() == "foo");

    ++it;
    BOOST_REQUIRE((it == end));
}

BOOST_AUTO_TEST_CASE(operator_)
{
    temp_file f;
    f << "[foo]\n"
      << "key1 = value1\n";

    ini_file ini { f.path() };

    BOOST_TEST(ini.size() == 2);

    auto const& foo = ini["foo"];
    BOOST_TEST(foo.size() == 1);

    auto const& bar = ini["bar"];
    BOOST_TEST(bar.empty());
}

BOOST_AUTO_TEST_SUITE_END() // ini_file_

} // namespace stream9::ini::testing
