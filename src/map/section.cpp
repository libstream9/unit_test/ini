#include <stream9/ini/map/ini_file.hpp>
#include <stream9/ini/error.hpp>

#include <boost/test/unit_test.hpp>

namespace stream9::ini::testing {

BOOST_AUTO_TEST_SUITE(section_)

BOOST_AUTO_TEST_CASE(name_)
{
    section const s { "foo" };

    BOOST_TEST(s.name() == "foo");
}

BOOST_AUTO_TEST_CASE(find_success_)
{
    section s { "" };
    s["key"] = "value";

    BOOST_TEST(s.find("key"));
}

BOOST_AUTO_TEST_CASE(find_fail_)
{
    section s { "" };
    s["key"] = "value";

    BOOST_TEST(!s.find("bar"));
}

BOOST_AUTO_TEST_CASE(find_const_success_)
{
    section s { "" };
    s["key"] = "value";

    auto const& cs = s;

    BOOST_TEST(cs.find("key"));
}

BOOST_AUTO_TEST_CASE(find_const_fail_)
{
    section s { "" };
    s["key"] = "value";

    auto const& cs = s;

    BOOST_TEST(!cs.find("bar"));
}

BOOST_AUTO_TEST_CASE(contains_success_)
{
    section s { "" };
    s["key"] = "value";

    BOOST_TEST(s.contains("key"));
}

BOOST_AUTO_TEST_CASE(contains_fail_)
{
    section s { "" };
    s["key"] = "value";

    BOOST_TEST(!s.contains("bar"));
}

BOOST_AUTO_TEST_CASE(size_0_)
{
    section s { "" };

    BOOST_TEST(s.size() == 0);
}

BOOST_AUTO_TEST_CASE(size_1_)
{
    section s { "" };
    s["foo"] = "value";

    BOOST_TEST(s.size() == 1);
}

BOOST_AUTO_TEST_CASE(size_2_)
{
    section s { "" };
    s["foo"] = "value";
    s["bar"] = "value";

    BOOST_TEST(s.size() == 2);
}

BOOST_AUTO_TEST_CASE(empty_success_)
{
    section s { "" };

    BOOST_TEST(s.empty());
}

BOOST_AUTO_TEST_CASE(empty_fail_)
{
    section s { "" };
    s["foo"] = "value";

    BOOST_TEST(!s.empty());
}

BOOST_AUTO_TEST_CASE(append_success_)
{
    section s { "" };

    BOOST_TEST(s.empty());

    s.append("key", "value");
    auto* const v = s.find("key");

    BOOST_TEST(s.size() == 1);
    BOOST_REQUIRE(v);
    BOOST_TEST(*v == "value");
}

BOOST_AUTO_TEST_CASE(append_fail_1_)
{
    section s { "" };

    BOOST_TEST(s.empty());

    s.append("key", "value1");

    BOOST_TEST(s.size() == 1);

    BOOST_CHECK_EXCEPTION(
        s.append("key", "value2"), ini::error,
            [](auto& e) {
                return e.code() == error_code::duplicate_key;
            } );

    BOOST_TEST(s.size() == 1);
}

BOOST_AUTO_TEST_CASE(append_fail_2_)
{
    section s { "" };

    BOOST_TEST(s.empty());

    BOOST_CHECK_EXCEPTION(
        s.append("", "value1"), ini::error,
            [](auto& e) {
                return e.code() == error_code::empty_key;
            } );

    BOOST_TEST(s.empty());
}

BOOST_AUTO_TEST_CASE(find_or_append_success_1_)
{
    section s { "" };

    s.append("foo", "bar");
    BOOST_REQUIRE(s.size() == 1);

    auto const& v = s.find_or_append("foo");
    BOOST_TEST(s.size() == 1);
    BOOST_TEST(v == "bar");
}

BOOST_AUTO_TEST_CASE(find_or_append_success_2_)
{
    section s { "" };

    BOOST_TEST(s.empty());

    auto const& v = s.find_or_append("foo");

    BOOST_TEST(s.size() == 1);
    BOOST_TEST(v == "");
}

BOOST_AUTO_TEST_CASE(find_or_append_failure_)
{
    section s { "" };

    auto check = [](auto& e) {
        return e.code() == error_code::empty_key;
    };

    BOOST_CHECK_EXCEPTION(s.find_or_append(""), ini::error, check);
}

BOOST_AUTO_TEST_CASE(erase_success_)
{
    section s { "" };

    s.append("foo", "bar");

    BOOST_TEST(s.find("foo"));
    BOOST_TEST(s.size() == 1);

    auto const ok = s.erase("foo");

    BOOST_TEST(ok);
    BOOST_TEST(!s.find("foo"));
    BOOST_TEST(s.empty());
}

BOOST_AUTO_TEST_CASE(erase_fail_1_)
{
    section s { "" };

    auto const ok = s.erase("foo");

    BOOST_TEST(!ok);
}

BOOST_AUTO_TEST_CASE(erase_fail_2_)
{
    section s { "" };

    s.append("foo", "bar");

    BOOST_TEST(s.find("foo"));
    BOOST_TEST(s.size() == 1);

    auto const ok = s.erase("bar");

    BOOST_TEST(!ok);
    BOOST_TEST(s.find("foo"));
    BOOST_TEST(s.size() == 1);
}

BOOST_AUTO_TEST_CASE(clear_)
{
    section s { "" };

    s.append("foo", "bar");

    BOOST_TEST(s.size() == 1);

    s.clear();

    BOOST_TEST(s.empty());
}

BOOST_AUTO_TEST_CASE(iterator_)
{
    section s { "" };

    s.append("key1", "value1");
    s.append("key2", "value2");

    auto it = s.begin();
    auto const end = s.end();

    BOOST_REQUIRE((it != end));
    BOOST_TEST(it->key == "key1");
    BOOST_TEST(it->value == "value1");

    ++it;

    BOOST_REQUIRE((it != end));
    BOOST_TEST(it->key == "key2");
    BOOST_TEST(it->value == "value2");

    ++it;

    BOOST_TEST((it == end));
}

BOOST_AUTO_TEST_CASE(subscript_operator_success_1_)
{
    section s { "" };

    s.append("foo", "bar");
    BOOST_REQUIRE(s.size() == 1);

    s["foo"] = "baz";
    auto* const v = s.find("foo");

    BOOST_TEST(s.size() == 1);
    BOOST_REQUIRE(v);
    BOOST_TEST(*v == "baz");
}

BOOST_AUTO_TEST_CASE(subscript_operator_success_2_)
{
    section s { "" };

    BOOST_TEST(s.empty());

    s["foo"] = "bar";
    auto* const v = s.find("foo");

    BOOST_TEST(s.size() == 1);
    BOOST_REQUIRE(v);
    BOOST_TEST(*v == "bar");
}

BOOST_AUTO_TEST_CASE(subscript_operator_failure_)
{
    section s { "" };

    auto check = [](auto& e) {
        return e.code() == error_code::empty_key;
    };

    BOOST_CHECK_EXCEPTION(s[""], ini::error, check);
}

BOOST_AUTO_TEST_CASE(stream_out_normal_)
{
    section s { "foo" };
    s["key"] = "value";

    std::ostringstream oss;
    oss << s;

    auto const expected = "[foo]\n"
                          "key = value\n";

    BOOST_TEST(oss.str() == expected);
}

BOOST_AUTO_TEST_CASE(stream_out_with_escape_)
{
    section s { "[[\txxx\tyyy\t]]" };
    s["[key=key]"] = ";value;value;";

    std::ostringstream oss;
    oss << s;

    auto const expected = "[\"[[\\txxx\\tyyy\\t]]\"]\n"
                          "\\[key\\=key] = \\;value\\;value\\;\n";

    BOOST_TEST(oss.str() == expected);
}

BOOST_AUTO_TEST_SUITE_END() // section_

} // namespace stream9::ini::testing
