#include <stream9/ini/map/ini_file.hpp>

#include <stream9/ini/error.hpp>

#include "../file.hpp"
#include "../json.hpp"

#include <iostream>

#include <boost/test/unit_test.hpp>

#include <stream9/json.hpp>

namespace stream9::ini::testing {

BOOST_AUTO_TEST_SUITE(parser_)

BOOST_AUTO_TEST_CASE(success_)
{
    temp_file f;
    f << "foo = bar\n"
      << "\n"
      << "[section1]\n"
      << "key1 = value1\n"
      << "\n"
      << "[section2]\n"
      << "key2 = value2\n";

    ini_file ini { f.path() };

    auto const expected = json::parse(R"(
        {
          "": {
            "foo": "bar"
          },
          "section1": {
            "key1": "value1"
          },
          "section2": {
            "key2": "value2"
          }
        }
        )");

    auto const& obj = to_json(ini);

    BOOST_TEST(obj == expected);
}

BOOST_AUTO_TEST_CASE(empty_section_name_1_)
{
    temp_file f;
    f << "[]\n"
      << "foo = baz\n"
      << "[section]\n"
      << "key = value\n";

    BOOST_CHECK_EXCEPTION(
        ini_file ini { f.path() },
        parse_error,
        [&](auto& e) {
            BOOST_TEST(e.code() == error_code::section_name_is_expected);
            BOOST_TEST(e.path() == f.path());
            BOOST_TEST(e.line_no() == 1);
            BOOST_TEST(e.position() == 1);
            return true;
        } );
}

BOOST_AUTO_TEST_CASE(empty_section_name_2_)
{
    temp_file f;
    f << "[]\n"
      << "foo = baz\n"
      << "[section]\n"
      << "key = value\n";

    auto&& [ini, errors] = ini::parse(f.path());

    auto const expected = json::parse(R"(
        {
          "": {},
          "section": {
            "key": "value"
          }
        }
        )");

    BOOST_TEST(to_json(ini) == expected);
    BOOST_REQUIRE(errors.size() == 1);
    BOOST_TEST(errors[0].code() == error_code::section_name_is_expected);
    BOOST_TEST(errors[0].path() == f.path());
    BOOST_TEST(errors[0].line_no() == 1);
    BOOST_TEST(errors[0].position() == 1);
}

BOOST_AUTO_TEST_CASE(quoted_section_name_)
{
    temp_file f;
    f << "[' section ']\n"
      << "key = value\n";

    auto&& [ini, errors] = ini::parse(f.path());

    auto const expected = json::parse(R"(
        {
          "": {},
          " section ": {
            "key": "value"
          }
        }
        )");

    BOOST_TEST(to_json(ini) == expected);
    BOOST_REQUIRE(errors.empty());
}

BOOST_AUTO_TEST_CASE(section_with_escape_sequence_)
{
    temp_file f;
    f << "[xxx\\tyyy]\n"
         "key = value";

    ini_file ini { f.path() };

    auto const expected = json::parse(
        "{ "
        "  \"\": {}, "
        "  \"xxx\\tyyy\": { \"key\": \"value\" } "
        "}" );

    auto const& obj = to_json(ini);

    BOOST_TEST(obj == expected);
}

BOOST_AUTO_TEST_CASE(invalid_section_name_1_)
{
    temp_file f;
    f << "[section1]\n"
      << "foo = bar\n"
      << "[section;]\n"
      << "key = value\n";

    BOOST_CHECK_EXCEPTION(
        ini_file ini { f.path() },
        parse_error,
        [&](auto& e) {
            BOOST_TEST(e.code() == error_code::invalid_section_name);
            BOOST_TEST(e.path() == f.path());
            BOOST_TEST(e.line_no() == 3);
            BOOST_TEST(e.position() == 8);
            return true;
        } );
}

BOOST_AUTO_TEST_CASE(invalid_section_name_2_)
{
    temp_file f;
    f << "[section1]\n"
      << "foo = bar\n"
      << "[section;]\n"
      << "key = value\n";

    auto&& [ini, errors] = ini::parse(f.path());

    auto const expected = json::parse(R"(
        {
          "": {},
          "section1": { "foo": "bar" }
        }
        )" );

    BOOST_TEST(to_json(ini) == expected);
    BOOST_REQUIRE(errors.size() == 1);
    BOOST_TEST(errors[0].code() == error_code::invalid_section_name);
    BOOST_TEST(errors[0].path() == f.path());
    BOOST_TEST(errors[0].line_no() == 3);
    BOOST_TEST(errors[0].position() == 8);
}

BOOST_AUTO_TEST_CASE(no_section_terminator_1_)
{
    temp_file f;
    f << "[section\n"
      << "name = value\n"
      << "[section2]\n"
      << "key = value\n";

    BOOST_CHECK_EXCEPTION(
        ini_file ini { f.path() },
        parse_error,
        [&](auto& e) {
            BOOST_TEST(e.code() == error_code::section_terminator_is_expected);
            BOOST_TEST(e.path() == f.path());
            BOOST_TEST(e.line_no() == 1);
            BOOST_TEST(e.position() == 8);
            return true;
        } );
}

BOOST_AUTO_TEST_CASE(no_section_terminator_2_)
{
    temp_file f;
    f << "[section\n"
      << "name = value\n"
      << "[section2]\n"
      << "key = value\n";

    auto&& [ini, errors] = ini::parse(f.path());

    auto const expected = json::parse(R"(
        {
          "": {},
          "section2": { "key": "value" }
        }
        )" );

    BOOST_TEST(to_json(ini) == expected);
    BOOST_REQUIRE(errors.size() == 1);
    BOOST_TEST(errors[0].code() == error_code::section_terminator_is_expected);
    BOOST_TEST(errors[0].path() == f.path());
    BOOST_TEST(errors[0].line_no() == 1);
    BOOST_TEST(errors[0].position() == 8);
}

BOOST_AUTO_TEST_CASE(no_new_line_after_section_1_)
{
    temp_file f;
    f << "[section] name1 = value1\n"
      << "name2 = value2";

    BOOST_CHECK_EXCEPTION(
        ini_file ini { f.path() },
        parse_error,
        [&](auto& e) {
            BOOST_TEST(e.code() == error_code::new_line_is_expected);
            BOOST_TEST(e.path() == f.path());
            BOOST_TEST(e.line_no() == 1);
            BOOST_TEST(e.position() == 10);
            return true;
        } );
}

BOOST_AUTO_TEST_CASE(duplicate_section_name_)
{
    temp_file f;
    f << "[section]\n"
      << "name1 = value1\n"
      << "[section]"
      << "name2 = value2";

    BOOST_CHECK_EXCEPTION(
        ini_file ini { f.path() },
        parse_error,
        [&](auto& e) {
            BOOST_TEST(e.code() == error_code::duplicate_section_name);
            BOOST_TEST(e.path() == f.path());
            BOOST_TEST(e.line_no() == 3);
            BOOST_TEST(e.position() == 1);
            return true;
        } );
}

BOOST_AUTO_TEST_CASE(no_new_line_after_section_2_)
{
    temp_file f;
    f << "[section] name1 = value1\n"
      << "name2 = value2";

    auto&& [ini, errors] = ini::parse(f.path());

    auto const expected = json::parse(R"(
        {
          "": {},
          "section": { "name2": "value2" }
        }
        )" );

    BOOST_TEST(to_json(ini) == expected);
    BOOST_REQUIRE(errors.size() == 1);
    BOOST_TEST(errors[0].code() == error_code::new_line_is_expected);
    BOOST_TEST(errors[0].path() == f.path());
    BOOST_TEST(errors[0].line_no() == 1);
    BOOST_TEST(errors[0].position() == 10);
}

BOOST_AUTO_TEST_CASE(quoted_key_)
{
    temp_file f;
    f << "' key ' = value\n";

    auto&& [ini, errors] = ini::parse(f.path());

    auto const expected = json::parse(R"(
        { "": { " key ": "value" } }
        )");

    BOOST_TEST(to_json(ini) == expected);
    BOOST_REQUIRE(errors.empty());
}

BOOST_AUTO_TEST_CASE(key_with_escape_sequence_)
{
    temp_file f;
    f << "\\=xxx\\tyyy\\= = value";

    ini_file ini { f.path() };

    auto const expected = json::parse(R"(
        { "": { "=xxx\tyyy=": "value" } }
    )");

    auto const& obj = to_json(ini);

    BOOST_TEST(obj == expected);
}

BOOST_AUTO_TEST_CASE(key_only_1_)
{
    temp_file f;
    f << "key1 = value1\n"
         "key2\n"
         "key3 =";

    BOOST_CHECK_EXCEPTION(
        ini_file ini { f.path() },
        parse_error,
        [&](auto& e) {
            BOOST_TEST(e.code() == error_code::key_value_separator_is_expected);
            BOOST_TEST(e.path() == f.path());
            BOOST_TEST(e.line_no() == 2);
            BOOST_TEST(e.position() == 4);
            return true;
        } );
}

BOOST_AUTO_TEST_CASE(key_only_2_)
{
    temp_file f;
    f << "key1 = value1\n"
         "key2\n"
         "key3 =";

    auto&& [ini, errors] = ini::parse(f.path());

    auto const expected = json::parse(R"(
        {
          "": {
            "key1": "value1",
            "key3": ""
          }
        }
        )");

    BOOST_TEST(to_json(ini) == expected);

    BOOST_REQUIRE(errors.size() == 1);
    BOOST_TEST(errors[0].code() == error_code::key_value_separator_is_expected);
    BOOST_TEST(errors[0].path() == f.path());
    BOOST_TEST(errors[0].line_no() == 2);
    BOOST_TEST(errors[0].position() == 4);
}

BOOST_AUTO_TEST_CASE(duplicate_key_1_)
{
    temp_file f;
    f << "foo = bar\n"
      << "foo = baz\n";

    BOOST_CHECK_EXCEPTION(
        ini_file ini { f.path() },
        parse_error,
        [&](auto& e) {
            BOOST_TEST(e.code() == error_code::duplicate_key);
            BOOST_TEST(e.path() == f.path());
            BOOST_TEST(e.line_no() == 2);
            BOOST_TEST(e.position() == 0);
            return true;
        } );
}

BOOST_AUTO_TEST_CASE(duplicate_key_2_)
{
    temp_file f;
    f << "foo = bar\n"
      << "foo = baz\n";

    auto&& [ini, errors] = ini::parse(f.path());

    auto const expected = json::parse(R"(
        {
          "": { "foo": "bar" }
        }
        )");

    BOOST_TEST(to_json(ini) == expected);

    BOOST_REQUIRE(errors.size() == 1);
    BOOST_TEST(errors[0].code() == error_code::duplicate_key);
    BOOST_TEST(errors[0].path() == f.path());
    BOOST_TEST(errors[0].line_no() == 2);
    BOOST_TEST(errors[0].position() == 0);
}

BOOST_AUTO_TEST_CASE(empty_key_1_)
{
    temp_file f;
    f << "    = bar\n"
         "foo = bar";

    BOOST_CHECK_EXCEPTION(
        ini_file ini { f.path() },
        parse_error,
        [&](auto& e) {
            BOOST_TEST(e.code() == error_code::empty_key);
            BOOST_TEST(e.path() == f.path());
            BOOST_TEST(e.line_no() == 1);
            BOOST_TEST(e.position() == 4);
            return true;
        } );
}

BOOST_AUTO_TEST_CASE(empty_key_2_)
{
    temp_file f;
    f << "    = bar\n"
         "foo = bar";

    auto&& [ini, errors] = ini::parse(f.path());

    auto const expected = json::parse(R"(
        {
          "": { "foo": "bar" }
        }
        )");

    BOOST_TEST(to_json(ini) == expected);

    BOOST_REQUIRE(errors.size() == 1);
    BOOST_TEST(errors[0].code() == error_code::empty_key);
    BOOST_TEST(errors[0].path() == f.path());
    BOOST_TEST(errors[0].line_no() == 1);
    BOOST_TEST(errors[0].position() == 4);
}

BOOST_AUTO_TEST_CASE(invalid_key_1a_)
{
    temp_file f;
    f << "key; = value\n"
      << "key2 = value2";

    BOOST_CHECK_EXCEPTION(
        ini_file ini { f.path() },
        parse_error,
        [&](auto& e) {
            BOOST_TEST(e.code() == error_code::key_value_separator_is_expected);
            BOOST_TEST(e.path() == f.path());
            BOOST_TEST(e.line_no() == 1);
            BOOST_TEST(e.position() == 3);
            return true;
        } );
}

BOOST_AUTO_TEST_CASE(invalid_key_1b_)
{
    temp_file f;
    f << "key; = value\n"
      << "key2 = value2";

    auto&& [ini, errors] = ini::parse(f.path());

    auto const expected = json::parse(R"(
        {
          "": { "key2": "value2" }
        }
        )");

    BOOST_TEST(to_json(ini) == expected);

    BOOST_REQUIRE(errors.size() == 1);
    BOOST_TEST(errors[0].code() == error_code::key_value_separator_is_expected);
    BOOST_TEST(errors[0].path() == f.path());
    BOOST_TEST(errors[0].line_no() == 1);
    BOOST_TEST(errors[0].position() == 3);
}

BOOST_AUTO_TEST_CASE(invalid_key_2a_)
{
    temp_file f;
    f << "key\n = value\n"
      << "key2 = value2";

    BOOST_CHECK_EXCEPTION(
        ini_file ini { f.path() },
        parse_error,
        [&](auto& e) {
            BOOST_TEST(e.code() == error_code::key_value_separator_is_expected);
            BOOST_TEST(e.path() == f.path());
            BOOST_TEST(e.line_no() == 1);
            BOOST_TEST(e.position() == 3);
            BOOST_TEST_MESSAGE(e.what());
            return true;
        } );
}

BOOST_AUTO_TEST_CASE(invalid_key_2b_)
{
    temp_file f;
    f << "key\n = value\n"
      << "key2 = value2";

    auto&& [ini, errors] = ini::parse(f.path());

    auto const expected = json::parse(R"(
        {
          "": { "key2": "value2" }
        }
        )");

    BOOST_TEST(to_json(ini) == expected);

    BOOST_TEST_REQUIRE(errors.size() == 2);
    BOOST_TEST(errors[0].code() == error_code::key_value_separator_is_expected);
    BOOST_TEST(errors[0].path() == f.path());
    BOOST_TEST(errors[0].line_no() == 1);
    BOOST_TEST(errors[0].position() == 3);

    BOOST_TEST(errors[1].code() == error_code::empty_key);
    BOOST_TEST(errors[1].path() == f.path());
    BOOST_TEST(errors[1].line_no() == 2);
    BOOST_TEST(errors[1].position() == 1);
}

BOOST_AUTO_TEST_CASE(quoted_value_)
{
    temp_file f;
    f << "key = \";value;\"\n";

    auto&& [ini, errors] = ini::parse(f.path());

    auto const expected = json::parse(R"(
        { "": { "key": ";value;" } }
        )");

    BOOST_TEST(to_json(ini) == expected);
    BOOST_REQUIRE(errors.empty());
}

BOOST_AUTO_TEST_CASE(value_with_escape_sequence_)
{
    temp_file f;
    f << "key = xxx\\tyyy";

    ini_file ini { f.path() };

    auto const expected = json::parse(
        "{ \"\": { \"key\": \"xxx\\tyyy\" } }" );

    auto const& obj = to_json(ini);

    BOOST_TEST(obj == expected);
}

BOOST_AUTO_TEST_CASE(value_with_continuation_)
{
    temp_file f;
    f << "key = line1\\\n"
      << "      line2";

    ini_file ini { f.path() };

    auto const expected = json::parse(R"(
        {
          "": { "key": "line1      line2" }
        }
        )");

    auto const& obj = to_json(ini);

    BOOST_TEST(obj == expected);
}

BOOST_AUTO_TEST_SUITE_END() // parser_

} // namespace stream9::ini::testing
